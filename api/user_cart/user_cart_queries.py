


INSERT_USER_CART = 'Insert into user_cart (user_id ,product_id ,quantity, added_date )'\
                    'values (%s,%s,%s,now())'

COUNT_PRODUCT_USER = 'Select count(*) from user_cart where user_id = %s and product_id =%s'


GET_PRICE = 'Select cast(product_price as CHAR) as price from user_cart '\
            'Join product_catalog on user_cart.product_id = product_catalog.product_id '\
            'where user_cart.product_id = %s'

GET_USER_CART = 'Select user_cart.product_id ,product_name,product_image,CONCAT(measurement," ",unit_type) as weight,'\
                'cast(product_price as CHAR) as price,brand_name,category_name,quantity  '\
                'from product_catalog '\
                'join user_cart on product_catalog.product_id = user_cart.product_id '\
                'join unit_types on product_catalog.unit_type_id = unit_types.unit_type_id ' \
                'join brands on product_catalog.brand_id = brands.brand_id ' \
                'join product_categories on product_catalog.product_category_id = product_categories.category_id ' \
                'where user_id = %s'

UPDATE_QUANTITY = 'Update user_cart set quantity = %s where product_id = %s and user_id = %s '

DELETE_FROM_CART = 'Delete from user_cart where product_id = %s and user_id = %s '