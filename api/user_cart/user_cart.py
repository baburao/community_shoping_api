from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.user_cart import user_cart_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertUserCart(generics.GenericAPIView):

    def post(self,request):

        def __init__(self):
            self.logger = logger(logmodules.USER_CART).get()
            self.error_logger = logger(logmodules.USER_CART_ERROR).get()
            self.logger.info("User Cart API Service Started")

            # Method Name                : post
            # Description                : Inserts User Cart
            # Created By                 : Satwika
            # Created Date               : 10 May 2019
            # Last Modified By           :
            # Last Modified Date         :
            # Modification Description   :

        try :
            cursor = connection.cursor()
            user_id = request.data['user_id']
            products = request.data['products']
            for prod in products:
                product_id = prod['product_id']
                quantity = prod['quantity']

                args1 = [user_id,product_id]
                res = QueryExecute.execute_many(args1,user_cart_queries.COUNT_PRODUCT_USER,cursor)
                if res > 0 :
                    if quantity > 0:
                        args = [quantity, product_id, user_id]
                        result = QueryExecute.insert_execute_many(args, user_cart_queries.UPDATE_QUANTITY, cursor)
                    else:
                        args = [product_id, user_id]
                        result = QueryExecute.insert_execute_many(args, user_cart_queries.DELETE_FROM_CART, cursor)
                else :
                    args = [user_id,product_id,quantity]
                    result = QueryExecute.insert_execute_many(args,user_cart_queries.INSERT_USER_CART,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class GetUserCart(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_CART).get()
        self.error_logger = logger(logmodules.USER_CART_ERROR).get()
        self.logger.info("User Cart API Service Started")

        # Method Name                : post
        # Description                : Return User Cart
        # Created By                 : Satwika
        # Created Date               : 10 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            cursor = connection.cursor()
            user_id = request.data['user_id']
            result = QueryExecute.get_json_many([user_id], user_cart_queries.GET_USER_CART,cursor)

            for r in result :
                product_id  = r['product_id']
                quantity = r['quantity']
                price = QueryExecute.execute_many([product_id], user_cart_queries.GET_PRICE, cursor)
                total_product_price = quantity * float(price)
                r['total_product_price'] = total_product_price

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(result))

class UpdateQuantity(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_CART).get()
        self.error_logger = logger(logmodules.USER_CART_ERROR).get()
        self.logger.info("User Cart API Service Started")

        # Method Name                : post
        # Description                : Update User Cart Quantity
        # Created By                 : Satwika
        # Created Date               : 10 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            user_id = request.data['user_id']
            product_id = request.data['product_id']
            quantity = request.data['quantity']

            if quantity > 0:
                args = [quantity,product_id,user_id]
                result = QueryExecute.insert_execute_many(args,user_cart_queries.UPDATE_QUANTITY,cursor)
            else :
                args = [product_id, user_id]
                result = QueryExecute.insert_execute_many(args, user_cart_queries.DELETE_FROM_CART,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class DeleteFromCart(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_CART).get()
        self.error_logger = logger(logmodules.USER_CART_ERROR).get()
        self.logger.info("User Cart API Service Started")

        # Method Name                : post
        # Description                : Delete From Cart
        # Created By                 : Satwika
        # Created Date               : 10 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            user_id = request.data['user_id']
            product_id = request.data['product_id']

            args = [product_id, user_id]
            result = QueryExecute.insert_execute(args, user_cart_queries.DELETE_FROM_CART)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse("Deleted")