from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.vendor import vendor_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.encode.encode_decode import *
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertVendor(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Inserts Vendor
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor =  connection.cursor()
        try :
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            password = request.data['password']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pan_no = request.data['pan_no']
            gst_no = request.data['gst_no']
            account_no = request.data['account_no']
            ifsccode = request.data['ifsccode']
            branch = request.data['branch']
            bankname = request.data['bankname']
            holdername = request.data['holdername']
            accounttype = request.data['accounttype']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']


            args = [email_id,pan_no]
            result1 = QueryExecute.execute_many(args,vendor_queries.VENDOR_INSERT_COUNT,cursor)
            if (result1> 0):
                return HttpResponse(QueryExecute.result_duplicate)

            if(gst_no != ''):
                result1 = QueryExecute.execute_many([gst_no], vendor_queries.VENDOR_GST_INSERT_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(QueryExecute.result_duplicate)

            pwds = encode(password)

            args = [first_name,last_name,phone_no,email_id,pwds,address,state_id,city_id,pincode,pan_no,account_no,accounttype,ifsccode,branch,bankname,holdername,gst_no,created_by,updated_by]

            result2 = QueryExecute.insert_execute_many(args,vendor_queries.INSERT_VENDOR,cursor)

            res = QueryExecute.execute_many([], vendor_queries.GET_LAST_VALUE, cursor)

            result = QueryExecute.get_json_many([res],vendor_queries.GET_VENDOR_BY_ID,cursor)
            result4 = QueryExecute.insert_execute_many( [res], vendor_queries.INSERT_VENDOR_PAYMENT, cursor )



        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse((json.dumps(result)))

class VendorLogin(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Vendor Login
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            # request.data = request.data['params']
            mobileNum = request.data['mobile_num']
            password = request.data['password']
            result1 = QueryExecute.execute_many([mobileNum], vendor_queries.MOBILENUM_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse(json.dumps("Invaild_UserName"))
            elif (result1 == 1):
                pwd = QueryExecute.execute_many([mobileNum],vendor_queries.GET_PASSWORD,cursor)
                if(password == decode(pwd)):
                    args =(mobileNum,pwd)
                    result = QueryExecute.get_json_many(args,vendor_queries.GET_INFO_BY_USER_PWD,cursor)
                    if(len(result) == 0):
                        return HttpResponse(json.dumps(("Invaild_UserName")))
                else :
                    return HttpResponse(json.dumps("Invalid username or password.Enter correct credentials"))
            else :
                return HttpResponse(json.dumps("Server Issue"))

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(result))


class GetVendorById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Returns Vendor by Id
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:

            vendor_id = request.data['vendor_id']
            result = QueryExecute.get_json([vendor_id], vendor_queries.GET_VENDOR_BY_ID)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse((json.dumps(result)))

class ChangeVendorPassword(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Change vendor password
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            email_id = request.data['email_id']

            result1 = QueryExecute.execute_many([email_id], vendor_queries.EMAIL_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse("Invaild UserName")
            elif (result1 == 1):
                old_password = request.data['old_password']
                pwd = QueryExecute.execute_many([email_id],vendor_queries.GET_PASSWORD,cursor)

                if (old_password != decode(pwd)):
                    return HttpResponse("Incorrect current password")

                new_password = request.data['new_password']

                pwds = encode(new_password)

                args = (pwds,email_id)
                result = QueryExecute.insert_execute_many(args,vendor_queries.UPDATE_PASSWORD,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class ForgetVendorPassword(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Forgets vendor password
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            email_id = request.data['email_id']

            result1 = QueryExecute.execute_many([email_id], vendor_queries.EMAIL_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse("Invaild UserName")
            elif (result1 == 1):
                password = request.data['password']
                pwds = encode(password)
                args = (pwds,email_id)
                result = QueryExecute.insert_execute_many(args,vendor_queries.UPDATE_PASSWORD,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            cursor.close()
            print(result)
            return HttpResponse(QueryExecute.result_update)


class GetAllVendor(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Returns All Vendor
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([], vendor_queries.GET_ALL_VENDOR)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class UpdateVendorDetails(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Update Vendor Details
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pan_no = request.data['pan_no']
            vnd_acctNo = request.data['account_no']
            vnd_acctype = request.data['accounttype']
            vnd_ifscCode = request.data['ifsccode']
            vnd_branch = request.data['branch']
            vnd_bankname = request.data['bankname']
            vnd_holdername = request.data['holdername']
            gst_no = request.data['gst_no']
            updated_by = request.data['updated_by']
            vendor_id = request.data['vendor_id']
            args = (pan_no, email_id,vendor_id)
            result1 = QueryExecute.execute_many(args,vendor_queries.VENDOR_UPDATE_COUNT,cursor)
            if (result1> 0):
                return HttpResponse(QueryExecute.result_duplicate)

            args = (gst_no, vendor_id)
            if (gst_no != 'NULL'):
                result1 = QueryExecute.execute_many(args, vendor_queries.VENDOR_GST_UPDATE_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(QueryExecute.result_duplicate)
            args = [first_name,last_name,phone_no,email_id,address,state_id,city_id,pincode,pan_no,vnd_acctNo,
                    vnd_acctype,vnd_ifscCode,vnd_branch,vnd_bankname,vnd_holdername,gst_no,updated_by,vendor_id]

            result = QueryExecute.insert_execute_many(args,vendor_queries.UPDATE_VENDOR_DETAILS,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))


class UpdateVendorStatus(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR).get()
        self.error_logger = logger(logmodules.VENDOR_ERROR).get()
        self.logger.info("Vendor API Service Started")

        # Method Name                : post
        # Description                : Update Vendor Status
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            is_active = request.data['is_active']
            vendor_id = request.data['vendor_id']


            args = (is_active,vendor_id)

            result = QueryExecute.insert_execute(args,vendor_queries.UPDATE_VENDOR_STATUS)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class UpdateVndprodprice(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR ).get()
        self.error_logger = logger( logmodules.VENDOR_ERROR ).get()
        self.logger.info( "Vendor API Service Started" )

    def post(self,request):
        try:
            vndId = request.data['vendor_id']
            vndPricesArray = request.data['prodPricesArray']
            for x in vndPricesArray:
                prodPrice=x['vendor_price']
                prodId = x['product_id']
                args = [prodPrice,prodId,vndId]
                result = QueryExecute.insert_execute(args,vendor_queries.UPDATE_VND_PROD_PRICE)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps( QueryExecute.result_update ) )
