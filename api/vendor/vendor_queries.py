


INSERT_VENDOR = "Insert into vendor_enrollment (first_name,last_name,phone_no,email_id,password,address,state_id,city_id," \
                "pincode,pan_no,accountno,accounttype,ifsccode,branch,bankname,holdername,gst_no,vendor_image,is_active,created_by," \
                "created_date,updated_by,updated_date,role) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'user.png','Y'" \
                ",%s,NOW(),%s,now(),'vendor')"

VENDOR_INSERT_COUNT = "Select count(*) from vendor_enrollment where email_id = %s and pan_no = %s"

VENDOR_GST_INSERT_COUNT = "Select count(gst_no) from vendor_enrollment where gst_no = %s"

GET_LAST_VALUE='select LAST_INSERT_ID() as id'

GET_VENDOR_BY_ID = "Select vendor_id,first_name,last_name,phone_no,email_id,address,ve.state_id,ve.city_id,pincode,pan_no," \
                   "gst_no,ve.accountno,ve.accounttype,ve.ifsccode,ve.branch,ve.bankname,ve.holdername,ve.vendor_image," \
                   "s.state_name,c.city_name " \
                   "from vendor_enrollment as ve " \
                   "join states as s on ve.state_id = s.state_id " \
                   "join cities as c on ve.city_id=c.city_id " \
                   "where vendor_id = %s "

EMAIL_COUNT = "Select count(*) from vendor_enrollment where email_id = %s "

GET_PASSWORD = "Select password from vendor_enrollment where phone_no = %s "

GET_INFO_BY_USER_PWD = "Select vendor_id,first_name,last_name,phone_no,email_id,address,ve.vendor_image,ve.state_id,ve.city_id,pincode," \
                       "pan_no,gst_no,role,s.state_name,c.city_name from vendor_enrollment as ve join states as s on " \
                       "ve.state_id = s.state_id join cities as c on ve.city_id=c.city_id where phone_no = %s and " \
                       "password = %s and ve.is_active = 'Y' "

UPDATE_PASSWORD = "update vendor_enrollment set password = %s where phone_no = %s"

GET_ALL_VENDOR = "Select vendor_id, CONCAT(first_name, ' ',last_name) as name,phone_no,email_id, ve.city_id,ve.state_id," \
                 "ve.accountno,ve.accounttype,ve.ifsccode,ve.branch,ve.bankname,ve.holdername, is_active,gst_no,pan_no," \
                 "address,s.state_name,c.city_name from vendor_enrollment as ve" \
                 " join states as s on ve.state_id = s.state_id join cities as c on ve.city_id=c.city_id "

UPDATE_VENDOR_DETAILS = "Update vendor_enrollment set first_name = %s,last_name = %s,phone_no = %s,email_id = %s,address = %s,"\
                        "state_id=%s,city_id= %s,pincode=%s, pan_no = %s,accountno = %s,accounttype=%s,ifsccode =%s," \
                        "branch = %s,bankname = %s,holdername = %s,gst_no =%s ,updated_by = %s, updated_date = now()" \
                        " Where vendor_id = %s"

VENDOR_UPDATE_COUNT = "Select count(*) from vendor_enrollment where pan_no = %s and email_id = %s and vendor_id != %s"

VENDOR_GST_UPDATE_COUNT = "Select count(gst_no) from vendor_enrollment where gst_no = %s and vendor_id != %s"

UPDATE_VENDOR_STATUS = "Update vendor_enrollment set is_active = %s "\
                        "Where vendor_id = %s"

INSERT_VENDOR_PAYMENT = 'INSERT INTO payment_settlement(vendor_id) VALUES(%s)'

UPDATE_VND_PROD_PRICE = 'UPDATE vendor_prod_mapping SET vendor_price =%s WHERE product_id=%s AND vendor_id = %s'

MOBILENUM_COUNT = 'SELECT count(phone_no) FROM vendor_enrollment WHERE phone_no = %s'


