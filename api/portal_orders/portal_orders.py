from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules
from api.portal_orders import portal_orders_querry
from lib.query_executer.queryExecuter import QueryExecute


# PORTAL VENDOR ORDERS
class GetPrtlorders(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.PORTAL_ORDERS).get()
        self.error_logger = logger( logmodules.PORTAL_ORDERS_ERROR ).get()
        self.logger.info( "GET_PORTAL_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Portal Pending Orders By Date
        # Created By                 : Harish
        # Created Date               : 13 JULY2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            VndId = request.data['vendor_id']
            FrmDt = request.data['from_date']
            TomDt = request.data['to_date']
            Status = request.data['status']
            UsrId = request.data['user_id']
            args = [FrmDt,TomDt];
            actquerry =portal_orders_querry.GET_PRTL_SHP_ORDS_DET_
            if (VndId != 'null' and VndId != '' and VndId != None):
                args.append( VndId )
                actquerry = actquerry + ' AND ogvnasgb.vendor_id = %s'
            if (UsrId != 'null'  and UsrId != None):
                args.append(UsrId)
                actquerry = actquerry + '   AND usrod.user_id = %s'
            if (Status != 'null'  and Status != None):
                if(Status == 'Pending'):
                    actquerry = actquerry + '  and ogvnasgb.order_vendor_status="ASSIGN" and usroddet.order_detail_status = "ASSIGNED"'
                elif(Status == 'Confirmed'):
                    actquerry = actquerry + '  and ogvnasgb.order_vendor_status="ACCEPT" and usroddet.order_detail_status = "PACKING"'
                elif (Status == 'Shipped'):
                    actquerry = actquerry + '  and ogvnasgb.order_vendor_status="CLOSED" and usroddet.order_detail_status = "SHIPPED"'
                else:
                    actquerry = actquerry + '  and ogvnasgb.order_vendor_status="CLOSED" and usroddet.order_detail_status = "DELIVERED"'

            finalqry = actquerry + ' GROUP BY usrod.user_id'
            result = QueryExecute.get_json( args,finalqry)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

# PORTAL VENDOR ORDERS DETAILS
class Getprtlorderdetails(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.PORTAL_ORDERS ).get()
        self.error_logger = logger( logmodules.PORTAL_ORDERS_ERROR ).get()
        self.logger.info( "GET_PORTAL_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Portal Orders Details By Date
        # Created By                 : Harish
        # Created Date               : 13 JULY2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            VndId = request.data['vendor_id']
            Usrid = request.data['user_id']
            Status = request.data['status']
            FrmDt = request.data['from_date']
            TomDt = request.data['to_date']
            args = [FrmDt,TomDt]
            actQuerry = portal_orders_querry.GET_PRTL_ORDS_DET_
            # if (VndId != 'null' and VndId != '' and VndId != None):
            #     args.append( VndId )
            #     actquerry = actQuerry + ' and ogvnasgb.vendor_id = %s'
            if (Usrid != 'null' and Usrid != None):
                args.append( Usrid )
                actQuerry = actQuerry + ' and usrod.user_id = %s '
            if (Status != 'null' and Status != None):
                if (Status == 'Pending'):
                    actQuerry = actQuerry + ' and usroddet.order_detail_status = "ASSIGNED" AND ogvnasgb.order_vendor_status = "ASSIGN"'
                elif (Status == 'Confirmed'):
                    actQuerry = actQuerry + ' and usroddet.order_detail_status = "PACKING" AND ogvnasgb.order_vendor_status = "ACCEPT"'
                elif (Status == 'Shipped'):
                    actQuerry = actQuerry + ' and usroddet.order_detail_status = "SHIPPED" AND ogvnasgb.order_vendor_status = "CLOSED"'
                else:
                    actQuerry = actQuerry + ' and usroddet.order_detail_status = "DELIVERED" AND ogvnasgb.order_vendor_status = "CLOSED"'
            finalqry = actQuerry + ' ORDER BY usroddet.delivery_date'
            result = QueryExecute.get_json( args, finalqry )
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps( result ) )

# PORTAL USER ORDERS
class GetPrtlUsrorders(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.PORTAL_ORDERS).get()
        self.error_logger = logger( logmodules.PORTAL_ORDERS_ERROR ).get()
        self.logger.info( "GET_PORTAL_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Portal User Orders
        # Created By                 : Harish
        # Created Date               : 06 Aug2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            FrmDt = request.data['from_date']
            TomDt = request.data['to_date']
            Status = request.data['status']
            UsrId = request.data['user_id']
            args = [FrmDt,TomDt];
            actquerry =portal_orders_querry.GET_PRTL_USR_ORDERS
            if (UsrId != 'null'  and UsrId != None):
                args.append(UsrId)
                actquerry = actquerry + '   and uo.user_id = %s'
            if (Status != 'null'  and Status != None):
                if(Status == 'Open'):
                    actquerry = actquerry + '  and uo.order_status ="Open" '
                elif(Status == 'In-Progress'):
                    actquerry = actquerry + '  and uo.order_status ="In-Progress"'
                else:
                    actquerry = actquerry + '  and uo.order_status ="CLOSED"'
            # if (OrdId != 'null' and OrdId != None):
            #     args.append( OrdId )
            #     actquerry = actquerry + '   and uo.order_id = %s'
            finalqry = actquerry
            print(finalqry)
            result = QueryExecute.get_json( args,finalqry)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

# PORTAL USER ORDERS DETAILS
class GetPrtlUsrorddet( generics.GenericAPIView ):
    def __init__(self):
        self.logger = logger( logmodules.PORTAL_ORDERS ).get()
        self.error_logger = logger( logmodules.PORTAL_ORDERS_ERROR ).get()
        self.logger.info( "GET_PORTAL_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Portal User  Orders Details
        # Created By                 : Harish
        # Created Date               : 06 Aug2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            OrdId = request.data['order_id']
            # OrdStatus = request.data['order_status']
            actQuerry = portal_orders_querry.GET_ORDER_STATUS
            args = [OrdId]
            OrdStatus = QueryExecute.get_json( args, actQuerry )
            print(OrdStatus[0])
            if(OrdStatus[0]['order_status'] =='OPEN'):
                args1 =[OrdId,'OPEN']
                finalQry = portal_orders_querry.GET_PRTL_USR_OPN_DET
                result = QueryExecute.get_json( args1, finalQry )
            else:
                finalQry = portal_orders_querry.GET_PRTL_USR_ORD_DET
                result = QueryExecute.get_json( args, finalQry )
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps( result ) )

