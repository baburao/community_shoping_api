
# NOT DISPLAYING CANCEL AND CANCELLED ORDER DETAILS OF VENDOR AND USER
GET_PRTL_SHP_ORDS_DET_='SELECT usrod.user_id,CONCAT(usrenrl.first_name,usrenrl.last_name) AS username,usroddet.order_detail_status,' \
                       ' count(usroddet.quantity)as noofitems, CAST(sum(vnprmap.vendor_price * usroddet.quantity) AS CHAR) AS total FROM' \
                       ' vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ON vnprmap.vendor_id =ogvnasgb.vendor_id  ' \
                       'AND ogvnasgb.product_id = vnprmap.product_id INNER  JOIN order_prod_group_vendor_details AS ogvndet ON' \
                       ' ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id AND ogvnasgb.order_vendor_status !="CANCEL" ' \
                       'INNER JOIN user_order_details AS usroddet  ON ogvndet.order_detail_id = usroddet.order_detail_id  INNER JOIN user_orders AS usrod ' \
                       'ON usroddet.order_id = usrod.order_id AND usroddet.order_detail_status!= "CANCELLED" INNER JOIN user_enrollment AS usrenrl ON ' \
                       'usrod.user_id =usrenrl.user_id WHERE ogvnasgb.created_date BETWEEN %s AND %s'

# NOT DISPLAYING CANCEL AND CANCELLED ORDER DETAILS OF VENDOR AND USER
GET_PRTL_ORDS_DET_= 'SELECT usrod.user_id,usroddet.order_detail_id,CAST(usroddet.delivery_date AS CHAR) AS delivery_date,' \
                    'usroddet.order_detail_status,usrod.order_id,ogvnasgb.order_prod_grouping_id,prodctg.product_name,' \
                    'CONCAT( prodctg.measurement,ut.unit_type) AS weight,usroddet.quantity,CAST(vnprmap.vendor_price as char) AS costperunit,' \
                    'CAST(vnprmap.vendor_price * usroddet.quantity AS CHAR) AS totalprice FROM  vendor_prod_mapping AS vnprmap INNER JOIN ' \
                    'order_prod_group_vendor_assign AS ogvnasgb ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.order_vendor_status !="CANCEL" ' \
                    'AND ogvnasgb.product_id = vnprmap.product_id INNER  JOIN order_prod_group_vendor_details AS ogvndet ON  ' \
                    'ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id INNER JOIN user_order_details AS usroddet ON' \
                    ' ogvndet.order_detail_id = usroddet.order_detail_id  INNER JOIN product_catalog  AS prodctg ON ogvnasgb.product_id = prodctg.product_id ' \
                    'INNER JOIN unit_types AS ut ON prodctg.unit_type_id = ut.unit_type_id INNER JOIN user_orders AS usrod ON usroddet.order_id = usrod.order_id ' \
                    'AND usroddet.order_detail_status!= "CANCELLED" INNER JOIN user_enrollment AS usrenrl ON usrod.user_id =usrenrl.user_id  WHERE ' \
                    'ogvnasgb.created_date BETWEEN %s AND %s '

# DISPLAYING PORTAL USER ORDERS
GET_PRTL_USR_ORDERS = 'SELECT CONCAT(ue.first_name,ue.last_name) AS username,uo.order_id,CAST(uo.total_amount AS CHAR) AS total ' \
                      'FROM user_enrollment AS ue INNER JOIN user_orders AS uo ON uo.user_id = ue.user_id WHERE uo.order_status !="CANCELLED" ' \
                      'AND uo.order_date BETWEEN %s AND %s'

# DISPLAYING PORTAL USER ORDERS DETAILS
GET_PRTL_USR_ORD_DET = 'SELECT CONCAT(ue.first_name,ue.last_name) AS username,uod.order_detail_id,uod.product_id,prodctg.product_name,' \
                       'b.brand_name,CONCAT(prodctg.measurement,ut.unit_type) AS weight,uod.quantity,CAST(uod.product_price AS CHAR) AS price,' \
                       'CAST(uod.total_price AS CHAR) AS total,CAST(uod.delivery_date AS CHAR) AS delivery_date,CAST(uod.delivery_time AS CHAR) AS delivery_time,' \
                       'uod.order_detail_status,CONCAT(venrl.first_name,venrl.last_name) AS vendor_name,opgva.order_vendor_status FROM user_enrollment AS ue' \
                       ' INNER JOIN user_orders AS uo ON uo.user_id = ue.user_id INNER JOIN user_order_details AS uod ON uo.order_id = uod.order_id INNER JOIN ' \
                       'order_prod_group_vendor_details AS opgvd ON opgvd.order_detail_id = uod.order_detail_id INNER JOIN order_prod_group_vendor_assign  AS opgva ' \
                       'ON opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id AND opgva.order_vendor_status !="CANCEL" INNER JOIN vendor_enrollment AS venrl ' \
                       'ON venrl.vendor_id = opgva.vendor_id  INNER JOIN product_catalog AS prodctg ON prodctg.product_id = uod.product_id INNER JOIN unit_types AS ut  ON' \
                       ' ut.unit_type_id = prodctg.unit_type_id INNER JOIN brands AS b ON b.brand_id = prodctg.brand_id ' \
                       'WHERE  uo.order_id = %s '

# DISPLAYING PORTAL USER ORDERS DETAILS WHEN STATUS OPEN
GET_PRTL_USR_OPN_DET ='SELECT CONCAT(ue.first_name,ue.last_name) AS username,uod.order_detail_id,uod.product_id,prodctg.product_name,' \
                      'b.brand_name,CONCAT(prodctg.measurement,ut.unit_type) AS weight,uod.quantity,CAST(uod.product_price AS CHAR) AS price,' \
                      'CAST(uod.total_price AS CHAR) AS total,CAST(uod.delivery_date AS CHAR) AS delivery_date,CAST(uod.delivery_time AS CHAR) AS delivery_time' \
                      ',uod.order_detail_status FROM user_enrollment AS ue INNER JOIN user_orders AS uo ON uo.user_id = ue.user_id ' \
                      'INNER JOIN user_order_details AS uod ON uo.order_id = uod.order_id  INNER JOIN product_catalog AS prodctg ON ' \
                      'prodctg.product_id = uod.product_id INNER JOIN unit_types AS ut  ON ut.unit_type_id = prodctg.unit_type_id INNER JOIN ' \
                      'brands AS b ON b.brand_id = prodctg.brand_id WHERE  uo.order_id =%s  AND uo.order_status = %s'

GET_ORDER_STATUS ='SELECT order_status FROM user_orders WHERE order_id = %s'