from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.product_categories import product_categories_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertCategory(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Inserts Product Category Names
    # Created By                 : Baburao M
    # Created Date               : 27 Mar 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            category_name = request.data['category_name']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']
            result1 = QueryExecute.execute_many([category_name],product_categories_queries.CATEGORY_INSERT_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))
            args = [category_name,created_by,updated_by]
            result = QueryExecute.insert_execute_many(args,product_categories_queries.INSERT_CATEGORY,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))


class GetAllCategory(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Returns Product Category Names
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],product_categories_queries.GET_ALL_CATEGORY)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetActiveCategory(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Returns Active Product Category Names where products mapped to vendor
    # Created By                 : HARISH
    # Created Date               : 10 JULY 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],product_categories_queries.GET_ACTIVE_CATEGORY)
        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetAllActiveCategory(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Returns Active Product Category Names
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],product_categories_queries.GET_ALL_ACTIVE_CATEGORY)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class GetusrprodCategory(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Returns Active Product Category Names
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],product_categories_queries.GET_USR_PROD_CATEGORY)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class GetCategoryById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Get Product Category By Id
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            category_id = request.data['category_id']
            result = QueryExecute.get_json([category_id],product_categories_queries.GET_CATEGORY_BY_ID)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class UpdateCategoryName(generics.GenericAPIView) :
    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Update Product Category Names
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            category_name = request.data['category_name']
            category_id = request.data['category_id']
            updated_by = request.data['updated_by']

            args = [category_name, category_id]
            result1 = QueryExecute.execute_many(args,product_categories_queries.CATEGORY_UPDATE_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            args = [category_name,updated_by, category_id]
            result = QueryExecute.insert_execute_many(args,product_categories_queries.UPDATE_CATEGORY,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))


class UpdateCategoryStatus(generics.GenericAPIView) :
    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATEGORY).get()
        self.error_logger = logger(logmodules.PRODUCT_CATEGORY_ERROR).get()
        self.logger.info("Product Category API Service Started")

    # Method Name                : post
    # Description                : Update Category Status
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            category_id = request.data['category_id']
            is_active = request.data['is_active']

            args = [is_active, category_id]
            result = QueryExecute.insert_execute(args,product_categories_queries.UPDATE_CATEGORY_STATUS)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))