


INSERT_CATEGORY='insert into product_categories (category_name,is_active,created_by,created_date,updated_by,updated_date)'\
            ' values (%s,"Y",%s,now(),%s,now())'

CATEGORY_INSERT_COUNT = 'Select count(category_name) from product_categories where category_name = %s'


GET_ALL_CATEGORY= 'Select category_id,category_name,is_active from product_categories ORDER BY category_id DESC;'

GET_ALL_ACTIVE_CATEGORY = 'Select category_id,category_name,is_active from product_categories  WHERE is_active = "Y" '

GET_ACTIVE_CATEGORY ='Select DISTINCT(prodctg.category_id),prodctg.category_name,prodctg.is_active from product_categories AS prodctg ' \
                     'INNER JOIN product_catalog AS prodcatlog ON prodctg.category_id = prodcatlog.product_category_id AND prodctg.is_active = "Y"' \
                     ' AND prodcatlog.is_active = "Y"   INNER JOIN brands AS b ON b.brand_id = prodcatlog.brand_id AND b.is_active = "Y" ' \
                     'ORDER BY category_id DESC'

GET_USR_PROD_CATEGORY = 'Select DISTINCT(prodctg.category_id),prodctg.category_name,prodctg.is_active FROM vendor_prod_mapping AS vndprodmap' \
                        ' INNER JOIN product_catalog  AS prodcatlog  ON vndprodmap.product_id = prodcatlog.product_id INNER JOIN product_categories AS prodctg' \
                        ' ON prodctg.category_id = prodcatlog.product_category_id AND prodctg.is_active = "Y" AND prodcatlog.is_active = "Y"' \
                        ' INNER JOIN brands AS b ON b.brand_id = prodcatlog.brand_id AND b.is_active = "Y" ORDER BY category_id DESC'

GET_CATEGORY_BY_ID = 'Select category_id,category_name from product_categories where category_id = %s'

CATEGORY_UPDATE_COUNT = 'Select count(category_name) from product_categories where category_name = %s and category_id != %s '

UPDATE_CATEGORY = 'Update product_categories set category_name = %s,updated_by = %s,updated_date = now()where category_id = %s '

UPDATE_CATEGORY_STATUS = 'Update product_categories set is_active = %s where category_id = %s '