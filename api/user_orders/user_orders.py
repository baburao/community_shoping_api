from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.user_orders import user_order_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules
import datetime;

class InsertUserOrder(generics.GenericAPIView):

    def post(self,request):

        def __init__(self):
            self.logger = logger(logmodules.USER_ORDER).get()
            self.error_logger = logger(logmodules.USER_ORDER_ERROR).get()
            self.logger.info("User orders API Service Started")

            # Method Name                : post
            # Description                : Inserts User orders
            # Created By                 : Satwika
            # Created Date               : 13 May 2019
            # Last Modified By           :
            # Last Modified Date         :
            # Modification Description   :

        try :
            cursor = connection.cursor()
            user_id = request.data['user_id']
            total_amount = request.data['total_amount']
            total_products = request.data['total_products']
            transaction_status = request.data['transaction_status']
            paymentid = request.data['payment_id']

            if(transaction_status == "SUCCESS") :
                args = [user_id,transaction_status,total_amount,total_products]
                result1 = QueryExecute.insert_execute_many(args,user_order_queries.INSERT_USER_ORDER,cursor)
                order_id = QueryExecute.execute_many([],user_order_queries.GET_LAST_VALUE,cursor)
                order_detail = request.data['order_detail']
                for order in order_detail:
                    product_id = order['product_id']
                    product_price = order['price']
                    quantity = order['qty']
                    product_discount = '0.00'
                    total_price = order['prod_total']
                    delivery_date = order['delivery_date']
                    delivery_time = order['delivery_time']
                    args = [order_id, product_id,product_price,quantity,product_discount,total_price,delivery_date,delivery_time]
                    result = QueryExecute.insert_execute_many(args, user_order_queries.INSERT_USER_ORDER_DETAILS, cursor)

                result2 = QueryExecute.insert_execute_many([paymentid, order_id, total_amount, user_id], user_order_queries.
                                                           INSERT_USER_ORDER_PAYMENTS, cursor)

                r = QueryExecute.insert_execute_many([user_id], user_order_queries.DELETE_USER_FROM_CART,cursor)

            else :
                return HttpResponse("UnSuccessful Transcation")

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class GetAllUserOrder(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_ORDER).get()
        self.error_logger = logger(logmodules.USER_ORDER_ERROR).get()
        self.logger.info("User Order API Service Started")

        # Method Name                : post
        # Description                : Return All User Order
        # Created By                 : Satwika
        # Created Date               : 13 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :

            result = QueryExecute.get_json([], user_order_queries.GET_ALL_USER_ORDER)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

class GetUserOrderById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_ORDER).get()
        self.error_logger = logger(logmodules.USER_ORDER_ERROR).get()
        self.logger.info("User Order API Service Started")

        # Method Name                : post
        # Description                : Returns User Order by id
        # Created By                 : Satwika
        # Created Date               : 13 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], user_order_queries.GET_USER_ORDER_BY_ID)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

class GetUserOrderDetailById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_ORDER).get()
        self.error_logger = logger(logmodules.USER_ORDER_ERROR).get()
        self.logger.info("User Order API Service Started")

        # Method Name                : post
        # Description                : Returns User Order Details by id
        # Created By                 : Satwika
        # Created Date               : 13 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :

            order_id = request.data['order_id']
            args = [order_id]
            result = QueryExecute.get_json(args, user_order_queries.GET_USER_ORDER_DETAIL_BY_ID)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(result))


class UpdateOrderStatus(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_ORDER).get()
        self.error_logger = logger(logmodules.USER_ORDER_ERROR).get()
        self.logger.info("User Order API Service Started")

        # Method Name                : post
        # Description                : Update User Status
        # Created By                 : Satwika
        # Created Date               : 13 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            user_id = request.data['user_id']
            order_status = request.data['order_status']
            args = [order_status,user_id]
            result = QueryExecute.insert_execute(args,user_order_queries.UPDATE_ORDER_STATUS)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(QueryExecute.result_update)

class GetUserOrdCntbyordId(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER_ORDER).get()
        self.error_logger = logger(logmodules.USER_ORDER_ERROR).get()
        self.logger.info("User Order API Service Started")

        # Method Name                : post
        # Description                : Returns User Orders Count by order id
        # Created By                 : Harish
        # Created Date               : 15 July 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :

            order_id = request.data['order_id']
            args = [order_id]
            result = QueryExecute.get_json(args, user_order_queries.GET_COUNT_ORDERS_BY_ORDER_ID)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(result))

def myconverter(o):
    if isinstance(o, datetime.date):
        return o.__str__()
