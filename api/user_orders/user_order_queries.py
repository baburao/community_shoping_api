


INSERT_USER_ORDER = 'Insert into user_orders (user_id ,order_date , transaction_status, total_amount,total_products )'\
                    'values (%s,now(),%s,%s,%s)'

GET_LAST_VALUE='select LAST_INSERT_ID() as id'

INSERT_USER_ORDER_DETAILS = 'Insert into user_order_details '\
                            '(order_id,product_id ,product_price ,quantity, product_discount,total_price,delivery_date,' \
                            'delivery_time )'\
                            'values (%s,%s,%s,%s,%s,%s,%s,%s)'

GET_ALL_USER_ORDER = 'Select order_id,user_id, order_date,order_status,transaction_status,total_products,cast(total_amount as CHAR) as total_amount '\
                     'from user_orders'

GET_USER_ORDER_BY_ID = 'Select order_id,user_id ,order_date ,order_status, transaction_status,total_products,' \
                       'cast(total_amount as CHAR) as total_amount '\
                       'from user_orders '\
                       'Where user_id = %s'

GET_USER_ORDER_DETAIL_BY_ID = 'Select order_detail_id,uod.order_id,pc.product_image,pc.product_id,pc.product_name ,cast(pc.product_price as CHAR) as price' \
                              ' ,quantity,product_discount, cast(total_price as CHAR) as total_price from user_order_details as uod' \
                              ' join user_orders as uo on uod.order_id=uo.order_id join product_catalog as pc on uod.product_id = pc.product_id' \
                              ' Where uod.order_id = %s'

UPDATE_ORDER_STATUS = 'Update user_orders set order_status =%s Where order_id = %s'

GET_LAST_VALUE='select LAST_INSERT_ID() as id'

DELETE_USER_FROM_CART = 'Delete from user_cart where user_id = %s '

INSERT_USER_ORDER_PAYMENTS = 'Insert into user_payments (paymentid, order_id, total_amount,user_id,payment_date)' \
                             'values (%s,%s,%s,%s,now())'

GET_COUNT_ORDERS_BY_ORDER_ID ='Select CONCAT(usrenrl.first_name,usrenrl.last_name ) AS username,uod.order_id,cast(uod.total_amount as CHAR) as total ' \
                              'from user_orders as uod INNER JOIN user_enrollment AS usrenrl ON usrenrl.user_id = uod.user_id ' \
                              'Where uod.order_id =%s'

GET_CNT_ORD_BY_ORD_ID_BY_USR_ID ='SELECT CONCAT(ue.first_name,ue.last_name) AS username,uo.order_id,CAST(uo.total_amount AS CHAR) AS total' \
                                 ' FROM user_enrollment AS ue INNER JOIN user_orders AS uo ON uo.user_id = ue.user_id WHERE ' \
                                 'uo.user_id = %s AND uo.order_status = %s AND uo.order_date BETWEEN  %s AND %s'


