



INSERT_VENDOR_PROD_MAP = 'Insert into vendor_prod_mapping (vendor_id ,product_id,vendor_price ,created_by,created_date, updated_by,updated_date)'\
                         'values (%s,%s,%s,%s,now(),%s,now())'

COUNT_PRODUCT_VENDOR = 'Select count(*) from vendor_prod_mapping where vendor_id = %s and product_id =%s'

GET_VENDOR_PROD_MAP = 'Select vendor_prod_mapping.product_id ,product_name,product_image,CONCAT(measurement," ",unit_type) as weight,' \
                      'cast(product_price as CHAR) as price,cast(vendor_price as CHAR) AS vendor_price,brand_name,category_name from product_catalog join ' \
                      'vendor_prod_mapping on product_catalog.product_id = vendor_prod_mapping.product_id join unit_types on ' \
                      'product_catalog.unit_type_id = unit_types.unit_type_id  join brands on product_catalog.brand_id = brands.brand_id' \
                      ' join product_categories on product_catalog.product_category_id = product_categories.category_id ' \
                      'where vendor_id = %s AND product_catalog.is_active = "Y"'

DELETE_VENDOR_PROD_MAP = 'DELETE FROM vendor_prod_mapping WHERE vendor_id = %s AND product_id = %s'



