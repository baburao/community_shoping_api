from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.vendor_prod_map import vendor_prod_map_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

# class InsertVendorProdMap(generics.GenericAPIView):
#     def post(self,request):
#
#         def __init__(self):
#             self.logger = logger(logmodules.VENDOR_PROD_MAP).get()
#             self.error_logger = logger(logmodules.VENDOR_PROD_MAP_ERROR).get()
#             self.logger.info("Vendor product map API Service Started")
#
#             # Method Name                : post
#             # Description                : Inserts Vendor product map
#             # Created By                 : Satwika
#             # Created Date               : 15 May 2019
#             # Last Modified By           :
#             # Last Modified Date         :
#             # Modification Description   :
#
#         try :
#             cursor = connection.cursor()
#             count =0
#             vendor_id = request.data['vendor_id']
#             created_by = request.data['created_by']
#             updated_by = request.data['updated_by']
#             products = request.data['products']
#             for prod in products:
#                 product_id = prod['product_id']
#                 args1 = [vendor_id,product_id]
#                 result1 = QueryExecute.execute_many(args1,vendor_prod_map_queries.COUNT_PRODUCT_VENDOR,cursor)
#                 if (result1 > 0):
#                     count= count+1
#                     return HttpResponse(QueryExecute.result_duplicate)
#             if(count ==0):
#                 for prod in products:
#                     product_id = prod['product_id']
#                     args = [vendor_id,product_id,created_by,updated_by]
#                     result = QueryExecute.insert_execute_many(args,vendor_prod_map_queries.INSERT_VENDOR_PROD_MAP,cursor)
#
#         except Exception as err:
#             http_err = traceback.format_exc()
#             self.error_logger.error(http_err)
#             return HttpResponse(err)
#
#         else:
#             cursor.close()
#             print(result)
#             return HttpResponse(json.dumps(QueryExecute.result_insert))


class InsertVendorProdMap(generics.GenericAPIView):
    def post(self,request):

        def __init__(self):
            self.logger = logger(logmodules.VENDOR_PROD_MAP).get()
            self.error_logger = logger(logmodules.VENDOR_PROD_MAP_ERROR).get()
            self.logger.info("Vendor product map API Service Started")

            # Method Name                : post
            # Description                : Inserts Vendor product map
            # Created By                 : Satwika
            # Created Date               : 15 May 2019
            # Last Modified By           :
            # Last Modified Date         :
            # Modification Description   :

        try :
            cursor = connection.cursor()
            vendor_id = request.data['vendor_id']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']
            products = request.data['products']
            for prod in products:
                product_id = prod['product_id']
                vendor_price = prod['vendor_price']
                args1 = [vendor_id,product_id]
                result1 = QueryExecute.execute_many(args1,vendor_prod_map_queries.COUNT_PRODUCT_VENDOR,cursor)
                if (result1 > 0):
                    return HttpResponse(QueryExecute.result_duplicate)
                args = [vendor_id,product_id,vendor_price,created_by,updated_by]
                result = QueryExecute.insert_execute_many(args,vendor_prod_map_queries.INSERT_VENDOR_PROD_MAP,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class GetVendorProdMap(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR_PROD_MAP).get()
        self.error_logger = logger(logmodules.VENDOR_PROD_MAP_ERROR).get()
        self.logger.info("Vendor product map API Service Started")

        # Method Name                : post
        # Description                : Return Vendor product map
        # Created By                 : Satwika
        # Created Date               : 15 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :

            vendor_id = request.data['vendor_id']
            result = QueryExecute.get_json([vendor_id], vendor_prod_map_queries.GET_VENDOR_PROD_MAP)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(result))

class DeleteVendorProduct(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PROD_MAP ).get()
        self.error_logger = logger( logmodules.VENDOR_PROD_MAP_ERROR ).get()
        self.logger.info( "Vendor product map API Service Started" )
        # Method Name                : post
        # Description                : Return Vendor product map
        # Created By                 : Harish
        # Created Date               : 21 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            vendor_id = request.data['vendor_id']
            product_id =request.data['product_id']
            args =[vendor_id, product_id]
            result = QueryExecute.get_json(args, vendor_prod_map_queries.DELETE_VENDOR_PROD_MAP)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


