from django.db import connection
from django.http.response import HttpResponse
from rest_framework import generics
from lib.logger.logger import logger
from lib.query_executer.queryExecuter import *
from lib.logger import logmodules
import json
import traceback
from api.issuetracker import issuetracker_querries


class InsertUsrIssue(generics.GenericAPIView):
      def __init__(self):
        self.logger = logger( logmodules.ISSUE_LOG ).get()
        self.error_logger = logger( logmodules.ISSUE_LOG_ERROR ).get()
        self.logger.info( "ISSUE_LOG API Service Started" )
        # Method Name                : post
        # Description                : Inserts Community
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

      def post(self,request):
        try:
           cursor = connection.cursor()
           usrorderid = request.data['usrOrdId']
           usrProdname = request.data['usrProdName']
           usrSubject = request.data['usrSubject']
           usrDescp = request.data['usrDescp']
           role = request.data['role']
           role_id = request.data['user_id']
           args = [usrorderid,usrProdname,usrSubject,usrDescp,role,role_id]
           result = QueryExecute.insert_execute_many( args, issuetracker_querries.INSERT_USRISSUE, cursor)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )
        else:
            cursor.close()
            print( result )
            return HttpResponse( json.dumps( QueryExecute.result_insert ) )
