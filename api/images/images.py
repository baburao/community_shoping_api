from django.http import HttpResponse
from django.utils.datetime_safe import datetime
from rest_framework import generics
# from MyDb1.MyDb1proj import settings
from community_shoping import settings
from django.core.files.storage import FileSystemStorage
from lib.query_executer.queryExecuter import *
import json


class Prodimageupload(generics.GenericAPIView):
    def post(self, request):
        try:
            myfile = request.FILES['uploads']
            data = str(settings.IMAGE_DIR)
            fs = FileSystemStorage(data)

            filename = fs.save(myfile.name, myfile)
            file_location = myfile.name

        except Exception as err:

            return HttpResponse(err)
        else:
            return HttpResponse(json.dumps(file_location))


class PortalCmtimageupload(generics.GenericAPIView):
    def post(self, request):
        try:

            myfile = request.FILES['uploads']
            data = str(settings.CMMT_IMAGE_DIR)
            fs = FileSystemStorage(data)

            filename = fs.save(myfile.name, myfile)
            file_location = myfile.name
        except Exception as err:

            return HttpResponse(err)
        else:
            return HttpResponse(json.dumps(file_location))

class Profileimageupload(generics.GenericAPIView):
    def post(self, request):
            try:
                myfile = request.FILES['uploads']
                data = str( settings.PROF_IMAGE_DIR )
                fs = FileSystemStorage( data )

                filename = fs.save( myfile.name, myfile )
                file_location = myfile.name

            except Exception as err:

                return HttpResponse( err )
            else:
                return HttpResponse( json.dumps( QueryExecute.result_update ) )


