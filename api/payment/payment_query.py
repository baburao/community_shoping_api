
# Description                : PAYMENT QUERYS
# Created By                 : BABURAO M
# Created Date               : 12 JUNE 2019

GET_PAYMENT_LIST = 'SELECT opgva.vendor_id,CAST(SUM(opgva.total_quantity * vndprod.vendor_price) AS CHAR) AS price FROM vendor_prod_mapping AS vndprod' \
                   ' INNER JOIN order_prod_group_vendor_assign AS opgva ON vndprod.vendor_id = opgva.vendor_id AND ' \
                   'vndprod.product_id = opgva.product_id INNER JOIN order_prod_group_vendor_details AS opgvd  ON ' \
                   'opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id JOIN user_order_details AS usroddet ON ' \
                   'usroddet.order_detail_id = opgvd.order_detail_id INNER JOIN payment_settlement AS payset ON' \
                   ' payset.vendor_id = opgva.vendor_id  WHERE usroddet.payment_status = "NOT-PAID"  GROUP BY opgva.vendor_id'

GET_PAYMENT_DETAILS = 'SELECT opgva.vendor_id,CAST(opgva.total_quantity * vndprod.vendor_price AS CHAR) AS price,usroddet.order_detail_id' \
                      ' FROM vendor_prod_mapping AS vndprod INNER JOIN order_prod_group_vendor_assign AS opgva ON vndprod.vendor_id = opgva.vendor_id' \
                      ' AND vndprod.product_id = opgva.product_id INNER JOIN order_prod_group_vendor_details AS opgvd  ON' \
                      ' opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id JOIN user_order_details AS usroddet ON ' \
                      'usroddet.order_detail_id = opgvd.order_detail_id INNER JOIN payment_settlement AS payset ON' \
                      ' payset.vendor_id = opgva.vendor_id  WHERE usroddet.payment_status = "NOT-PAID" AND opgva.vendor_id = %s '

INSERT_PAYMENTS = 'insert into payment_settlement (vendor_id,amount,created_by,created_date,updated_by,updated_date)'\
            ' values (%s,%s,%s,now(),%s,now())'

GET_PAYMENT_AMOUNT = 'Select payable_amount,payment_id from payment_settlement where vendor_id = %s'

UPDATE_VENDOR_PAYMENTS = 'Update payment_settlement set balance_amount = %s,updated_by =%s,updated_date = now() where vendor_id = %s'


GET_LAST_VALUE='select LAST_INSERT_ID() as id'

INSERT_ADMIN_PAYMENTS = 'insert into admin_payments (amount,chequeno,bankname,acno,bankacname,branchname,ifsccode,' \
                          ' depositedate,refno,description,vendor_id,payment_type_id)values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'

ADMIN_PAYMENTS_LIST = 'select ap.id,concat(ve.first_name,ve.last_name) as name,spt.payment_type,cast(ap.amount as CHAR) AS amount,ap.depositedate ' \
                      'from admin_payments as ap join vendor_enrollment as ve on ap.vendor_id = ve.vendor_id join ' \
                      'st_payment_type as spt on spt.payment_type_id = ap.payment_type_id group by ap.id desc'

GET_BALANCE_AMOUNT = 'SELECT CAST(payset.balance_amount AS CHAR) AS balance_amount,payset.vendor_id,CONCAT(vndernl.first_name,vndernl.last_name) AS vendor_name ' \
                     'FROM payment_settlement AS payset INNER JOIN vendor_enrollment AS vndernl ON payset.vendor_id = vndernl.vendor_id'

UPDATE_USER_ORDER_DETAILS = 'UPDATE user_order_details SET payment_status="PAID" WHERE order_detail_id = %s'

GET_VENDOR_BALANCE_AMOUNT = 'SELECT CAST(balance_amount AS CHAR) AS balance FROM payment_settlement WHERE vendor_id = %s'

