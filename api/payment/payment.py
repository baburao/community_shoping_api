
# Created By                 : BABURAO M
# Created Date               : 12 JUNE 2019
# Last Modified By           :
# Last Modified Date         :
# Modification Description   :


from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.payment import payment_query
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules
import datetime

class GetAllPayments( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.PAYMENT).get()
        self.error_logger = logger(logmodules.PAYMENT_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:

            result = QueryExecute.get_json([], payment_query.GET_PAYMENT_LIST)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetAllPaymentsDetails( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.PAYMENT).get()
        self.error_logger = logger(logmodules.PAYMENT_ERROR).get()
        self.logger.info("Payment Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : HARISH
        # Created Date               : 05 JULY 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            vndId = request.data['vendor_id']
            result = QueryExecute.get_json([vndId],payment_query.GET_PAYMENT_DETAILS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetBalanceAmount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.PAYMENT).get()
        self.error_logger = logger(logmodules.PAYMENT_ERROR).get()
        self.logger.info("Payment Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : HARISH
        # Created Date               : 05 JULY 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            args = []
            result = QueryExecute.get_json(args,payment_query.GET_BALANCE_AMOUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result))


class InsertPayments(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PAYMENT).get()
        self.error_logger = logger(logmodules.PAYMENT_ERROR).get()
        self.logger.info("Insert Payments For Vendors")

        # Method Name                : post
        # Description                : Inserts Payments
        # Created By                 : Baburao
        # Created Date               : 13/06 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            vendor_id = request.data['vendor_id']
            amount = request.data['amount']

            created_by = "1"
            updated_by = "1"
            count = QueryExecute.execute_many([vendor_id], payment_query.PAYMENT_INSERT_COUNT, cursor)
            if (count > 0):
                vendor_data = QueryExecute.get_json([vendor_id], payment_query.GET_PAYMENT_AMOUNT)
                payment_id = vendor_data[0]['payment_id']
                vendor_amt = vendor_data[0]['amount']
                total_amount = float(vendor_amt)+float(amount);
                args = [total_amount,updated_by,vendor_id]
                result = QueryExecute.insert_execute_many(args,payment_query.UPDATE_VENDOR_PAYMENTS,cursor)
                payment_dtls = request.data['payment_dtls']
                for paymentdtl in payment_dtls:
                    order_detail_id = paymentdtl['order_id']
                    product_id = paymentdtl['product_id']
                    amount = paymentdtl['price']
                    args = [payment_id,order_detail_id, product_id, amount,updated_by]
                    result = QueryExecute.insert_execute_many(args, payment_query.INSERT_PAYMENTS_DETAILS, cursor)

            else:
                args = [vendor_id, amount, created_by, updated_by]
                result = QueryExecute.insert_execute_many(args, payment_query.INSERT_PAYMENTS, cursor)
                payment_id = QueryExecute.execute_many([], payment_query.GET_LAST_VALUE, cursor)
                payment_dtls = request.data['payment_dtls']
                for paymentdtl in payment_dtls:
                    order_detail_id = paymentdtl['order_id']
                    product_id = paymentdtl['product_id']
                    amount = paymentdtl['price']
                    args = [payment_id, order_detail_id, product_id, amount, updated_by]
                    result = QueryExecute.insert_execute_many(args, payment_query.INSERT_PAYMENTS_DETAILS, cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class InsertAdminPayments(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PAYMENT).get()
        self.error_logger = logger(logmodules.PAYMENT_ERROR).get()
        self.logger.info("InSert Admin Payments")

        # Method Name                : post
        # Description                : Inserts Payments
        # Created By                 : Baburao
        # Created Date               : 13/06 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            amount = request.data['amount']
            chequeno = request.data['chequeno']
            bankname = request.data['bankname']
            acno = request.data['acno']
            bankacname = request.data['bankacname']
            branchname = request.data['branchname']
            ifsccode = request.data['ifsccode']
            depositedate = request.data['depositedate']
            refno = request.data['refno']
            description = request.data['description']
            vendor_id = request.data['vendor_id']
            payment_type_id = request.data['payment_type_id']
            updated_by = request.data['updated_by']
            orddetId = request.data['order_detail_id']
            totamt = request.data['totAmt']
            args = [amount,chequeno, bankname, acno,bankacname, branchname, ifsccode,depositedate,refno,description,vendor_id,payment_type_id]
            result = QueryExecute.insert_execute_many(args, payment_query.INSERT_ADMIN_PAYMENTS, cursor)
            DBbalamt = QueryExecute.get_json( [vendor_id], payment_query.GET_VENDOR_BALANCE_AMOUNT)

            balance_amount = float(totamt)+float(DBbalamt[0]['balance']) - float(amount);
            # balance_amount = float( amount ) - float(  totamt );

            args = [balance_amount,updated_by, vendor_id]
            result = QueryExecute.insert_execute_many(args, payment_query.UPDATE_VENDOR_PAYMENTS, cursor)

            for x in orddetId:
                result = QueryExecute.insert_execute_many( [x], payment_query.UPDATE_USER_ORDER_DETAILS, cursor )


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))


class GetAminPayments( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.PAYMENT).get()
        self.error_logger = logger(logmodules.PAYMENT_ERROR).get()
        self.logger.info("GetAminPayments Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:

            result = QueryExecute.get_json([], payment_query.ADMIN_PAYMENTS_LIST)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

def myconverter(o):
    if isinstance(o, datetime.date):
        return o.__str__()