


INSERT_BRAND='insert into brands (brand_name,is_active,created_by,created_date,updated_by,updated_date)'\
            ' values (%s,"Y",%s,now(),%s,now())'

BRAND_INSERT_COUNT = 'Select count(brand_name) from brands where brand_name = %s'

GET_ALL_BRAND = 'Select brand_id,brand_name,is_active from brands ORDER BY brand_id DESC;'

GET_BRAND_BY_ID = 'Select brand_id,brand_name from brands where brand_id = %s'

BRAND_UPDATE_COUNT = 'Select count(brand_name) from brands where brand_name = %s and brand_id != %s '

UPDATE_BRAND = 'Update brands set brand_name = %s,updated_by = %s,updated_date = now()where brand_id = %s '

UPDATE_BRAND_STATUS = 'Update brands set is_active = %s where brand_id = %s '
