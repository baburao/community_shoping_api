from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.brand import brand_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertBrand(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.BRAND).get()
        self.error_logger = logger(logmodules.BRAND_ERROR).get()
        self.logger.info("Brand API Service Started")

        # Method Name                : post
        # Description                : Inserts Brand
        # Created By                 : Satwika
        # Created Date               : 27 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            brand_name = request.data['brand_name']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']

            result1 = QueryExecute.execute_many([brand_name],brand_queries.BRAND_INSERT_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))
            args = [brand_name,created_by,updated_by]
            result = QueryExecute.insert_execute_many(args,brand_queries.INSERT_BRAND,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))


class GetAllBrand(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.BRAND).get()
        self.error_logger = logger(logmodules.BRAND_ERROR).get()
        self.logger.info("Brand API Service Started")

    # Method Name                : post
    # Description                : Returns Brand Name
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],brand_queries.GET_ALL_BRAND)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class GetBrandById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.BRAND).get()
        self.error_logger = logger(logmodules.BRAND_ERROR).get()
        self.logger.info("Brand API Service Started")

    # Method Name                : post
    # Description                : Returns Brand By Id
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            brand_id = request.data['brand_id']
            result = QueryExecute.get_json([brand_id],brand_queries.GET_BRAND_BY_ID)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class UpdateBrandName(generics.GenericAPIView) :

    def __init__(self):
        self.logger = logger(logmodules.BRAND).get()
        self.error_logger = logger(logmodules.BRAND_ERROR).get()
        self.logger.info("Brand API Service Started")

        # Method Name                : post
        # Description                : Update Brand Names
        # Created By                 : Satwika
        # Created Date               : 27 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            brand_name = request.data['brand_name']
            brand_id = request.data['brand_id']
            updated_by = request.data['updated_by']

            args = [brand_name, brand_id]
            result1 = QueryExecute.execute_many(args,brand_queries.BRAND_UPDATE_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(QueryExecute.result_duplicate)

            args = [brand_name,updated_by, brand_id]
            result = QueryExecute.insert_execute_many(args,brand_queries.UPDATE_BRAND,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))


class UpdateBrandStatus(generics.GenericAPIView) :

    def __init__(self):
        self.logger = logger(logmodules.BRAND).get()
        self.error_logger = logger(logmodules.BRAND_ERROR).get()
        self.logger.info("Brand API Service Started")

    # Method Name                : post
    # Description                : Update Brand Status
    # Created By                 : Satwika
    # Created Date               : 27 Apr 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :

    def post(self,request):
        try :
            brand_id = request.data['brand_id']
            is_active = request.data['is_active']

            args = [is_active, brand_id]
            result = QueryExecute.insert_execute(args,brand_queries.UPDATE_BRAND_STATUS)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))