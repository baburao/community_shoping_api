



INSERT_PRODUCT = 'Insert into product_catalog (product_name ,product_code ,product_description,product_image,product_category_id ,'\
                 'product_type,brand_id ,measurement ,unit_type_id ,product_price ,hsn_code , '\
                 'is_active,created_by,created_date,updated_by,updated_date)'\
                 ' values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,"Y",%s,now(),%s,now())'


PRODUCT_CODE_INSERT_COUNT = 'Select count(product_code) from product_catalog where product_code = %s OR product_name = %s'


GET_ALL_PRODUCT='Select pc.product_id,pcg.category_id,cast("1" as char) as qty,pc.product_name,pc.product_image,CONCAT(pc.measurement," ",ut.unit_type) as weight,' \
                'cast(product_price as CHAR) as price,pc.product_type,cast(product_price as CHAR) as prod_total,pc.is_active,pcg.category_name,b.brand_name FROM product_menu AS pm' \
                ' INNER JOIN product_catalog   as pc ON pm.product_type_id = pc.product_type join product_categories as pcg on pc.product_category_id = pcg.category_id' \
                ' join unit_types as ut on pc.unit_type_id = ut.unit_type_id join brands as b on pc.brand_id = b.brand_id AND b.is_active = "Y" JOIN vendor_prod_mapping as vpm ' \
                'on pc.product_id = vpm.product_id  WHERE pc.is_active = "Y" GROUP BY pc.product_id'

# GET_ALL_PRODUCT = 'Select pc.product_id,pcg.category_id,cast("1" as char) as qty,pc.product_name,pc.product_image,CONCAT(pc.measurement," ",ut.unit_type)' \
#                   ' as weight,cast(product_price as CHAR) as price,cast(product_price as CHAR) as prod_total,pc.is_active,pcg.category_name,' \
#                   'b.brand_name from product_catalog as pc join product_categories as pcg on pc.product_category_id = pcg.category_id' \
#                   ' join unit_types as ut on pc.unit_type_id = ut.unit_type_id join brands as b on pc.brand_id = b.brand_id AND b.is_active = "Y"' \
#                   ' join vendor_prod_mapping as vpm on pc.product_id = vpm.product_id  WHERE pc.is_active = "Y" GROUP BY pc.product_id'

GET_ALL_PRODUCT_BY_ID = 'Select product_id,pct.category_name,ut.unit_type,product_name ,product_code ,product_description,' \
                        'pc.product_type,product_image,pc.product_category_id , b.brand_name,b.brand_id ,measurement ,ut.unit_type_id ,' \
                        'cast(product_price as CHAR) as price ,hsn_code from product_catalog as pc join product_categories as pct' \
                        ' on pc.product_category_id = pct.category_id join brands as b on b.brand_id = pc.brand_id join unit_types as ut' \
                        ' on ut.unit_type_id = pc.unit_type_id where product_id = %s'

UPDATE_PRODUCT = 'Update product_catalog set product_name = %s ,product_code = %s ,product_description = %s,product_image =%s, '\
                 'product_category_id =%s ,product_type = %s,brand_id = %s ,measurement = %s ,unit_type_id = %s ,product_price = %s ,hsn_code =%s ,'\
                 'updated_by = %s ,updated_date = now() '\
                 'where product_id = %s'

PRODUCT_CODE_UPDATE_COUNT = 'Select count(product_name) from product_catalog where product_name=%s AND product_id !=%s'

UPDATE_PRODUCT_STATUS = 'Update product_catalog set is_active = %s where product_id = %s '

GET_PRODUCT_BY_CATEGORY_ID ='Select product_id ,product_name,product_image,CONCAT(measurement," ",unit_type) as weight,'\
                            'cast(product_price as CHAR) as price,brand_name,category_name  '\
                            'from product_catalog '\
                            'join unit_types on product_catalog.unit_type_id = unit_types.unit_type_id ' \
                            'join brands on product_catalog.brand_id = brands.brand_id ' \
                            'join product_categories on product_catalog.product_category_id = product_categories.category_id ' \
                            'where product_catalog.product_category_id = %s'

GET_PRODUCT_BY_BRAND_ID ='Select product_id ,product_name,product_image,CONCAT(measurement," ",unit_type) as weight,'\
                            'cast(product_price as CHAR) as price,brand_name,category_name  '\
                            'from product_catalog '\
                            'join unit_types on product_catalog.unit_type_id = unit_types.unit_type_id ' \
                            'join brands on product_catalog.brand_id = brands.brand_id ' \
                            'join product_categories on product_catalog.product_category_id = product_categories.category_id ' \
                            'where product_catalog.brand_id = %s'

ADMIN_GET_ALL_PRODUCT = 'Select pc.product_id,pcg.category_id,cast("1" as char) as qty,pc.product_name,pc.product_image,CONCAT(pc.measurement," ",ut.unit_type)' \
                  ' as weight,cast(product_price as CHAR) as price,cast(product_price as CHAR) as prod_total,pc.is_active,pcg.category_name,' \
                  'b.brand_name from product_catalog as pc join product_categories as pcg on pc.product_category_id = pcg.category_id' \
                  ' join unit_types as ut on pc.unit_type_id = ut.unit_type_id join brands as b on pc.brand_id = b.brand_id' \
                        ' ORDER BY pc.product_id DESC;'

GET_PROD_CAT_MENU_WITH_CTG='SELECT (pcg.category_name),pm.product_type_id,pm.product_type_name,pcg.category_id FROM product_menu AS pm' \
                           ' INNER JOIN product_catalog  as pc ON pm.product_type_id = pc.product_type join product_categories as pcg on ' \
                           'pc.product_category_id = pcg.category_id join unit_types as ut on pc.unit_type_id = ut.unit_type_id join brands as b' \
                           ' on pc.brand_id = b.brand_id AND b.is_active = "Y" join vendor_prod_mapping as vpm on pc.product_id = vpm.product_id  ' \
                           'WHERE pc.is_active = "Y" AND pm.product_type_name = %s GROUP BY pcg.category_id'

GET_ALL_PRODUCT_MENUS = ' SELECT product_type_id,product_type_name FROM product_menu'