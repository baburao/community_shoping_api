from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.product_catalog import product_catalog_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertProduct(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Inserts Product Catalog
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            product_name = request.data['product_name']
            product_code = request.data['product_code']
            product_description = request.data['product_description']
            product_image = request.data['imgURL']
            product_category_id = request.data['product_category_id']
            product_type = request.data['product_type']
            brand_id = request.data['brand_id']
            measurement = request.data['measurement']
            unit_type_id = request.data['unit_type_id']
            product_price = request.data['product_price']
            hsn_code = request.data['hsn_code']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']
            agrs1 = [product_code,product_name]
            result1 = QueryExecute.execute_many(agrs1, product_catalog_queries.PRODUCT_CODE_INSERT_COUNT, cursor)
            if (result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            args = [product_name,product_code,product_description,product_image,product_category_id,product_type,brand_id,
                    measurement,unit_type_id,product_price,hsn_code,created_by,updated_by]
            result = QueryExecute.execute_many(args,product_catalog_queries.INSERT_PRODUCT,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))


class GetAllProducts(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Returns All Product Catalog
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],product_catalog_queries.GET_ALL_PRODUCT)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class AdminGetAllProducts(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Returns All Product Catalog
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],product_catalog_queries.ADMIN_GET_ALL_PRODUCT)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))



class GetProductById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Returns Product Catalog By Id
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try :
            product_id = request.data["product_id"]
            result = QueryExecute.get_json([product_id],product_catalog_queries.GET_ALL_PRODUCT_BY_ID)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetProductByCategory(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Returns Product Catalog By Category
        # Created By                 : Satwika
        # Created Date               : 17 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try :
            product_category_id = request.data["product_category_id"]
            result = QueryExecute.get_json([product_category_id],product_catalog_queries.GET_PRODUCT_BY_CATEGORY_ID)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetProductByBrand(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Returns Product Catalog By Brand
        # Created By                 : Satwika
        # Created Date               : 17 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try :
            brand_id = request.data["brand_id"]
            result = QueryExecute.get_json([brand_id],product_catalog_queries.GET_PRODUCT_BY_BRAND_ID)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class UpdateProductDetails(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Update Product Catalog Details
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            product_name = request.data['product_name']
            product_code = request.data['product_code']
            product_description = request.data['product_description']
            product_image = request.data['imgURL']
            product_category_id = request.data['product_category_id']
            product_type = request.data['product_type']
            brand_id = request.data['brand_id']
            measurement = request.data['measurement']
            unit_type_id = request.data['unit_type_id']
            product_price = request.data['product_price']
            hsn_code = request.data['hsn_code']
            product_id = request.data['product_id']
            updated_by = request.data['updated_by']

            args = [product_name,product_id]
            result1 = QueryExecute.execute_many(args, product_catalog_queries.PRODUCT_CODE_UPDATE_COUNT, cursor)
            if (result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            args = [product_name,product_code,product_description,product_image,product_category_id,product_type,brand_id,
                    measurement,unit_type_id,product_price,hsn_code,updated_by,product_id]
            result = QueryExecute.execute_many(args,product_catalog_queries.UPDATE_PRODUCT,cursor)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class UpdateProductStatus(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PRODUCT_CATALOG).get()
        self.error_logger = logger(logmodules.PRODUCT_CATALOG_ERROR).get()
        self.logger.info("Product Catalog API Service Started")

        # Method Name                : post
        # Description                : Update Product Catalog Status
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):

        try :
            product_id = request.data['product_id']
            is_active = request.data['is_active']

            args = [is_active,product_id]
            result = QueryExecute.insert_execute(args,product_catalog_queries.UPDATE_PRODUCT_STATUS)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class GetprodcatalogMenuwithCtg(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.PRODUCT_CATALOG ).get()
        self.error_logger = logger( logmodules.PRODUCT_CATALOG_ERROR ).get()
        self.logger.info( "Product Catalog API Service Started" )
        # Method Name                : post
        # Description                : Display Product Menu with Categories
        # Created By                 : Harish
        # Created Date               : 19 Sept 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            product_type_name = request.data['product_type_name']
            result = QueryExecute.get_json( [product_type_name], product_catalog_queries.GET_PROD_CAT_MENU_WITH_CTG )


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps( result ) )

class GetprodMenuNames(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.PRODUCT_CATALOG ).get()
        self.error_logger = logger( logmodules.PRODUCT_CATALOG_ERROR ).get()
        self.logger.info( "Product Catalog API Service Started" )
        # Method Name                : post
        # Description                : GET ALL Product Menu NAMES
        # Created By                 : Harish
        # Created Date               : 19 Sept 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            result = QueryExecute.get_json( [], product_catalog_queries.GET_ALL_PRODUCT_MENUS )


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps( result ) )