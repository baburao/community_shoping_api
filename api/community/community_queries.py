# INSERTING COMMUNITIES WITH VALUES
# InsertCommunity = "INSERT INTO community_enrolment VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,NOW(),%s,NOW())"

# INSERTING COMMUNITIES WITHOUT VALUES
AddCommunity = "INSERT INTO community_enrolment(community_name,registration_no,total_blocks,total_flats,phone_no,email_id,"\
               "address,state_id,city_id,pincode,pan_no,gst_no,community_image,contact_person,mobile_no,designation,"\
               "contact_email_id,is_active,created_by,created_date,updated_by,updated_date)VALUES"\
               "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'Y',%s,Now(),%s,Now())"

checkingDup = "SELECT count(*) FROM  community_enrolment WHERE community_name = %s or registration_no =%s"

COMMUNITY_GST_INSERT_COUNT = "Select count(gst_no) from community_enrolment where gst_no = %s"

COMMUNITY_PINCODE_COUNT = "Select count(pincode) from community_enrolment where community_name = %s and state_id = %s " \
                          "and city_id = %s and pincode = %s"

COMMUNITY_PAN_INSERT_COUNT = "Select count(pan_no) from community_enrolment where pan_no = %s"

# GET COMMUNITY by Id
GET_COMMUNITIY_BY_ID = "SELECT community_id,community_name,registration_no,total_blocks,total_flats,phone_no,email_id,address,state_id,"\
               "city_id,pincode,pan_no,gst_no,community_image,contact_person,mobile_no,designation,contact_email_id "\
               "from community_enrolment "\
               "WHERE community_id=%s;"

GET_ALL_COMMUNITIY = "SELECT community_id,community_name,CAST(total_flats AS CHAR) AS total_flats ,email_id,city_id,mobile_no,is_active "\
                     "from community_enrolment "
# ACTIVE COMMUNITIES
GET_Active_COMMUNITIY = "SELECT community_id,community_name,total_flats,email_id,state_id,city_id,pincode,mobile_no," \
                        "is_active from community_enrolment WHERE  state_id = %s and city_id=%s and pincode=%s and " \
                        "is_active ='Y' "

# UPDATING COMMUNITIES
UpdateCommunity = "UPDATE community_enrolment SET community_name=%s,registration_no=%s,total_blocks=%s,total_flats=%s,phone_no=%s,"\
                  "email_id=%s,address=%s,state_id=%s,city_id=%s,pincode=%s,pan_no=%s,gst_no=%s,community_image=%s,"\
                  "contact_person=%s,mobile_no=%s,designation=%s,contact_email_id=%s,updated_by=%s,updated_date=NOW()"\
                  " WHERE community_id=%s;"

checkingDup_UPDATE = "SELECT count(*) FROM  community_enrolment"\
                     " WHERE community_name = %s or registration_no =%s"

COMMUNITY_GST_UPDATE_COUNT = "Select count(gst_no) from community_enrolment where gst_no = %s"

COMMUNITY_PAN_UPDATE_COUNT = "Select count(pan_no) from community_enrolment where pan_no = %s"

# DELETING COMMUNITIES
UPDATE_COMMUNITY_STATUS ="UPDATE community_enrolment SET is_active = %s"\
                        " WHERE community_id=%s"



