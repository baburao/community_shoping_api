from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.community import community_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsrtCommunity( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.BRAND).get()
        self.error_logger = logger(logmodules.BRAND_ERROR).get()
        self.logger.info("Community API Service Started")

        # Method Name                : post
        # Description                : Inserts Community
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            cursor = connection.cursor()
            # cmtId = request.data['CmtId']
            community_name = request.data['community_name']
            registration_no = request.data['registration_no']
            total_blocks = request.data['total_blocks']
            total_flats = request.data['total_flats']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pincode1 = int(pincode)
            pan_no = request.data['pan_no']
            gst_no = request.data['gst_no']
            community_image = request.data['community_image']
            contact_person = request.data['contact_person']
            mobile_no = request.data['mobile_no']
            designation = request.data['designation']
            contact_email_id = request.data['contact_email_id']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']

            args = [community_name, registration_no]
            result1 = QueryExecute.execute_many(args, community_queries.checkingDup, cursor)
            if (result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))
            result1 = QueryExecute.execute_many([community_name,state_id,city_id,pincode1], community_queries.COMMUNITY_PINCODE_COUNT, cursor)
            if(result1 > 0):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))
            if (pan_no != 'NULL'):
                result1 = QueryExecute.execute_many([pan_no], community_queries.COMMUNITY_PAN_INSERT_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            if (gst_no != 'NULL'):
                result1 = QueryExecute.execute_many([gst_no], community_queries.COMMUNITY_GST_INSERT_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            args = [community_name, registration_no,total_blocks, total_flats, phone_no, email_id, address, state_id, city_id,
                    pincode,pan_no, gst_no,community_image, contact_person, mobile_no, designation,
                    contact_email_id, created_by, updated_by]
            result = QueryExecute.insert_execute_many( args, community_queries.AddCommunity,cursor )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class GetCommunityById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.COMMUNITY).get()
        self.error_logger = logger(logmodules.COMMUNITY_ERROR).get()
        self.logger.info("Community API Service Started")

        # Method Name                : post
        # Description                : Returns Community By Id
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try:
            community_id = request.data['community_id']
            result = QueryExecute.get_json([community_id], community_queries.GET_COMMUNITIY_BY_ID)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse((json.dumps(result)))

class GetAllCommunity(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.COMMUNITY).get()
        self.error_logger = logger(logmodules.COMMUNITY_ERROR).get()
        self.logger.info("Community API Service Started")

        # Method Name                : post
        # Description                : Returns All Community
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try:
            result = QueryExecute.get_json([], community_queries.GET_ALL_COMMUNITIY)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetActiveCommunity(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.COMMUNITY).get()
        self.error_logger = logger(logmodules.COMMUNITY_ERROR).get()
        self.logger.info("Community API Service Started")

        # Method Name                : post
        # Description                : Returns All Community
        # Created By                 : Harish
        # Created Date               : 14 June 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try:
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pincode1 = int(pincode)
            result = QueryExecute.get_json([state_id,city_id,pincode1], community_queries.GET_Active_COMMUNITIY)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result))

class UpdateCommunity(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.COMMUNITY).get()
        self.error_logger = logger(logmodules.COMMUNITY_ERROR).get()
        self.logger.info("Community API Service Started")

        # Method Name                : post
        # Description                : Update Community
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            cursor = connection.cursor()

            community_id = request.data['community_id']
            community_name = request.data['community_name']
            registration_no = request.data['registration_no']
            total_blocks = request.data['total_blocks']
            total_flats = request.data['total_flats']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pan_no = request.data['pan_no']
            gst_no = request.data['gst_no']
            community_image = request.data['community_image']
            contact_person = request.data['contact_person']
            mobile_no = request.data['mobile_no']
            designation = request.data['designation']
            contact_email_id = request.data['contact_email_id']
            updated_by = request.data['updated_by']

            args = [community_name, registration_no]
            result1 = QueryExecute.execute_many(args, community_queries.checkingDup_UPDATE, cursor)
            if (result1 > 1):
                return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            if (pan_no != 'NULL'):
                args = [pan_no]
                result1 = QueryExecute.execute_many(args, community_queries.COMMUNITY_PAN_UPDATE_COUNT, cursor)
                if (result1 > 1):
                    return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            if (gst_no != 'NULL'):
                args = [gst_no]
                result1 = QueryExecute.execute_many(args, community_queries.COMMUNITY_GST_UPDATE_COUNT, cursor)
                if (result1 > 1):
                    return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            args =[community_name, registration_no,total_blocks, total_flats, phone_no, email_id, address, state_id, city_id,
                    pincode,pan_no, gst_no,community_image, contact_person, mobile_no, designation,
                    contact_email_id,updated_by,community_id]

            finalres = QueryExecute.insert_execute_many( args, community_queries.UpdateCommunity, cursor)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(finalres)
            return HttpResponse(json.dumps(QueryExecute.result_update))


class UpdateCommunityStatus(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.COMMUNITY).get()
        self.error_logger = logger(logmodules.COMMUNITY_ERROR).get()
        self.logger.info("Community API Service Started")

        # Method Name                : post
        # Description                : Update Community Status
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try:
            community_id = request.data['community_id']
            is_active = request.data['is_active']

            args = [is_active,community_id]
            result = QueryExecute.insert_execute(args, community_queries.UPDATE_COMMUNITY_STATUS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

