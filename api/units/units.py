from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.units import units_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertUnit(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.UNIT).get()
        self.error_logger = logger(logmodules.UNIT_ERROR).get()
        self.logger.info("Unit API Service Started")

        # Method Name                : post
        # Description                : Inserts Unit
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            unit_type = request.data['unit_type']
            result1 = QueryExecute.execute_many([unit_type],units_queries.UNIT_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(json.dumps( QueryExecute.result_duplicate ))

            result = QueryExecute.insert_execute_many([unit_type],units_queries.INSERT_UNIT,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))


class GetAllUnit(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.UNIT).get()
        self.error_logger = logger(logmodules.UNIT_ERROR).get()
        self.logger.info("Unit API Service Started")

        # Method Name                : post
        # Description                : Return All Unit
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try :
            result = QueryExecute.get_json([],units_queries.GET_ALL_UNIT)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class UpdateUnit(generics.GenericAPIView) :

    def __init__(self):
        self.logger = logger(logmodules.UNIT).get()
        self.error_logger = logger(logmodules.UNIT_ERROR).get()
        self.logger.info("Unit API Service Started")

        # Method Name                : post
        # Description                : Update Unit
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            unit_type = request.data['unit_type']
            unit_type_id = request.data['unit_type_id']

            args = [unit_type, unit_type_id]
            result1 = QueryExecute.execute_many(args,units_queries.UNIT_UPDATE_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(QueryExecute.result_duplicate)


            result = QueryExecute.insert_execute_many(args,units_queries.UPDATE_UNIT,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))