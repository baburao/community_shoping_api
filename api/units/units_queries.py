

INSERT_UNIT='insert into unit_types (unit_type) values (%s)'

UNIT_COUNT = 'Select count(unit_type) as cnt from unit_types where unit_type = %s'

GET_ALL_UNIT = 'Select unit_type_id,unit_type from unit_types'

UPDATE_UNIT = 'Update unit_types set unit_type = %s where unit_type_id = %s '

UNIT_UPDATE_COUNT = 'Select count(unit_type) as cnt from unit_types where unit_type = %s and unit_type_id != %s '