from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.community import community_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules
from api.order_grouping import order_groping_querries

class GetOverallOrders(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_GROUPING).get()
        self.error_logger = logger(logmodules.ORDER_GROUPING_ERROR).get()
        self.logger.info("ORDER_GROUPING API Service Started")

        # Method Name                : post
        # Description                : Returns over all Orders
        # Created By                 : Harish
        # Created Date               : 28 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try:
            result = QueryExecute.get_json([], order_groping_querries.GET_OVERALL_ORDERS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse( json.dumps( result ) )

class GetOrdersByprodId(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.ORDER_GROUPING ).get()
        self.error_logger = logger( logmodules.ORDER_GROUPING_ERROR ).get()
        self.logger.info( "Orders By ProdId API Service Started" )
        # Method Name                : post
        # Description                : Returns Orders By ProdId
        # Created By                 : Harish
        # Created Date               : 29 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            prodId = request.data['product_id']
            args = [prodId]
            result = QueryExecute.get_json(args, order_groping_querries.GET_ORDER_DETAILS_BY_PRODID)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse( json.dumps( result ) )

class GetVndByProdId(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.ORDER_GROUPING ).get()
        self.error_logger = logger( logmodules.ORDER_GROUPING_ERROR ).get()
        self.logger.info( "Orders By ProdId API Service Started" )
        # Method Name                : post
        # Description                : Returns Vendors List By Product Id
        # Created By                 : Harish
        # Created Date               : 29 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            prodId = request.data['product_id']
            args = [prodId]
            result = QueryExecute.get_json( args, order_groping_querries.GET_VENDORS_BY_PRODUCT_ID )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps( result ) )

class VendorprodPrice(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.ORDER_GROUPING ).get()
        self.error_logger = logger( logmodules.ORDER_GROUPING_ERROR ).get()
        self.logger.info( "Vendor Product Price API Service Started" )
        # Method Name                : post
        # Description                : Returns Orders By ProdId
        # Created By                 : Harish
        # Created Date               : 24 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            vndId = request.data['vendor_id']
            prodId = request.data['product_id']
            args = [vndId,prodId]
            result = QueryExecute.get_json(args, order_groping_querries.GET_VENDOR_PRODUCT_PRICE)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse( json.dumps( result ) )




