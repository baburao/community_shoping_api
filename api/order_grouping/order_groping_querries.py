GET_OVERALL_ORDERS = 'SELECT cast(sum(user_order_details.quantity) AS char) AS quantity, ' \
                     'user_order_details.product_id, product_catalog.product_name FROM ((user_orders INNER JOIN user_order_details ON' \
                     ' user_orders.order_id = user_order_details.order_id ) INNER JOIN product_catalog ON' \
                     ' product_catalog.product_id = user_order_details.product_id)  WHERE (user_orders.order_status = "OPEN"' \
                     ' OR user_orders.order_status = "In-Progress") AND user_order_details.order_detail_status = "OPEN"' \
                     ' group BY product_id'

GET_ORDER_DETAILS_BY_PRODID = 'SELECT DISTINCT(od.order_detail_id),od.order_id, CAST(o.order_date AS CHAR) AS order_date,CONCAT(usr.first_name,usr.last_name) AS user_name,' \
                              'prod.product_id,prod.product_name,od.quantity,cast(od.product_price as char) as cost_price' \
                              ' FROM ((((user_order_details as od ' \
                              'right join user_orders AS o ON o.order_id = od.order_id) INNER JOIN user_enrollment AS usr' \
                              ' ON o.user_id = usr.user_id ) INNER JOIN product_catalog AS prod ON od.product_id = prod.product_id)' \
                              ' INNER JOIN vendor_prod_mapping AS vdprodmap ON prod.product_id = vdprodmap.product_id) ' \
                              'WHERE  od.product_id =%s AND od.order_detail_status = "OPEN"'


GET_VENDORS_BY_PRODUCT_ID = 'SELECT vendor_enrollment.vendor_id,CONCAT(vendor_enrollment.first_name,vendor_enrollment.last_name)' \
                            'AS name,product_catalog.product_name,product_catalog.product_id FROM ' \
                            '((vendor_prod_mapping INNER JOIN vendor_enrollment  ON vendor_prod_mapping.vendor_id = vendor_enrollment.vendor_id) ' \
                            'INNER JOIN product_catalog ON vendor_prod_mapping.product_id = product_catalog.product_id) ' \
                            'WHERE vendor_prod_mapping.product_id = %s AND vendor_enrollment.is_active = "Y"'

GET_VENDOR_ORDER_DETAILS = 'SELECT order_prod_group_vendor_assign.order_prod_grouping_id,vendor_assign_detail_id,' \
                           'user_order_details.order_detail_id FROM ((order_prod_group_vendor_details INNER  JOIN ' \
                           'order_prod_group_vendor_assign ON ' \
                           'order_prod_group_vendor_details.order_prod_grouping_id = order_prod_group_vendor_assign.order_prod_grouping_id)' \
                           ' INNER JOIN user_order_details ON order_prod_group_vendor_details.order_detail_id = user_order_details.order_detail_id)' \
                           'WHERE order_prod_group_vendor_assign.vendor_id = %s'

GET_VENDOR_PRODUCT_PRICE = 'SELECT CAST(vendor_price AS CHAR) AS vendor_price FROM vendor_prod_mapping WHERE vendor_id = %s AND product_id = %s'

