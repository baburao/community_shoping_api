
INSERT_CITY='insert into cities(city_name,state_id) values(%s,%s)'

CITY_INSERT_COUNT = 'Select count(city_name) as cnt from cities '\
                    'join states on cities.state_id = states.state_id '\
                    'where city_name = %s and cities.state_id = %s'

CITY_UPDATE_COUNT = 'Select count(city_name) as cnt from cities '\
                    'where city_name = %s and state_id = %s and city_id != %s'

CITY_DETAILS_BY_CITY_ID = 'select city_name,state_id from cities where city_id=%s'

UPDATE_CITYNAME = "Update cities set city_name = %s where city_id = %s and state_id = %s"

GET_ALL_CITY = "select city_id,city_name from cities"

GET_CITY_BY_STATE = "select city_id,city_name from cities where state_id = %s"