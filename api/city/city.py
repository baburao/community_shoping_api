from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.city import city_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertCity(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.CITY).get()
        self.error_logger = logger(logmodules.CITY_ERROR).get()
        self.logger.info("City API Service Started")

        # Method Name                : post
        # Description                : Inserts City
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :

            city_name = request.data['city_name']
            state_id = request.data['state_id']

            args = [city_name, state_id]
            result1 = QueryExecute.execute_many(args,city_queries.CITY_INSERT_COUNT,cursor)
            if (result1> 0):
                return HttpResponse(QueryExecute.result_duplicate)

            result = QueryExecute.insert_execute_many(args,city_queries.INSERT_CITY,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class GetAllCities(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.CITY).get()
        self.error_logger = logger(logmodules.CITY_ERROR).get()
        self.logger.info("City API Service Started")

        # Method Name                : post
        # Description                : Returns All City
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :

            result = QueryExecute.get_json([],city_queries.GET_ALL_CITY)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetCityDetailById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.CITY).get()
        self.error_logger = logger(logmodules.CITY_ERROR).get()
        self.logger.info("City API Service Started")

        # Method Name                : post
        # Description                : Return City By Id
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            city_id = request.data['city_id']
            args = [city_id]
            result = QueryExecute.get_json(args,city_queries.CITY_DETAILS_BY_CITY_ID)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetCitiesbyState(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.CITY).get()
        self.error_logger = logger(logmodules.CITY_ERROR).get()
        self.logger.info("City API Service Started")

        # Method Name                : post
        # Description                : Returns city by State id
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            state_id = request.data['state_id']
            result = QueryExecute.get_json([state_id],city_queries.GET_CITY_BY_STATE)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))


class UpdateCityName(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.CITY).get()
        self.error_logger = logger(logmodules.CITY_ERROR).get()
        self.logger.info("City API Service Started")

        # Method Name                : post
        # Description                : Update City Name
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            city_name = request.data['city_name']
            state_id = request.data['state_id']
            city_id = request.data['city_id']

            args = [city_name, state_id, city_id]
            result1 = QueryExecute.execute_many(args,city_queries.CITY_UPDATE_COUNT,cursor)
            if (result1 > 0):
                return HttpResponse(QueryExecute.result_duplicate)

            args = (city_name, city_id, state_id)
            result = QueryExecute.insert_execute_many(args,city_queries.UPDATE_CITYNAME,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))   