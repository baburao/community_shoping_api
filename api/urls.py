"""
Name                       : URL Configuration
Description                : The urlpatterns list routes URLs to views
Created By                 : Venkat G
Created Date               : 04-04-2019
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""
from django.conf.urls import url

from api.portal_orders.portal_orders import *
from api.state.state import *
from api.city.city import *
from api.vendor.vendor import *
from api.community.community import *
from api.users.users import *
from api.units.units import *
from api.brand.brand import *
from api.product_categories.product_categories import *
from api.product_catalog.product_catalog import *
from api.images.images import *
from api.user_cart.user_cart import *
from api.user_orders.user_orders import *
from api.vendor_prod_map.vendor_prod_map import *
from api.portal_admin.portal_admin import *
from api.issuetracker.issuetracker import InsertUsrIssue
from api.order_grouping.order_grouping import *
from api.ordertracking.ordertracking import *
from api.vendor_prod_assign.vendor_prod_assign import *
from api.payment.payment import *

urlpatterns = [


    #states
    url(r'^insertstate/$', InsertState.as_view(), name="insertState"),
    url(r'^getallstate/$', GetAllState.as_view(), name="getAllState"),
    url(r'^getstatebyid/$', GetStateById.as_view(), name="getStateById"),
    url(r'^updatestatename/$', UpdateStateName.as_view(), name="updateStateName"),

    #cities
    url(r'^insertcity/$', InsertCity.as_view(), name="insertCity"),
    url(r'^getallcities/$', GetAllCities.as_view(), name="getAllCities"),
    url(r'^getcitydetailbyid/$', GetCityDetailById.as_view(), name="getCityDetailById"),
    url(r'^updatecityname/$', UpdateCityName.as_view(), name="updateCityName"),
    url(r'^getcitiesbystate/$', GetCitiesbyState.as_view(), name="getCitiesbyState"),
    #vendor
    url(r'^insertvendor/$', InsertVendor.as_view(), name="insertVendor"),
    url(r'^getvendorbyid/$', GetVendorById.as_view(), name="getVendorById"),
    url(r'^vendorlogin/$', VendorLogin.as_view(), name="vendorLogin"),
    url(r'^forgetvendorpassword/$', ForgetVendorPassword.as_view(), name="forgetVendorPassword"),
    url(r'^changevendorpassword/$', ChangeVendorPassword.as_view(), name="changeVendorPassword"),
    url(r'^getallvendor/$', GetAllVendor.as_view(), name="getAllVendor"),
    url(r'^updatevendordetails/$', UpdateVendorDetails.as_view(), name="updateVendorDetails"),
    url(r'^updatevendorstatus/$', UpdateVendorStatus.as_view(), name="updateVendorStatus"),

    #community
    url(r'^insrtcommunity/$', InsrtCommunity.as_view(), name="insrtCommunity"),
    url(r'^getcommunitybyid/$', GetCommunityById.as_view(), name="getCommunityById"),
    url(r'^getallcommunity/$', GetAllCommunity.as_view(), name="getAllCommunity"),
    url(r'^updatecommunity/$', UpdateCommunity.as_view(), name="updateCommunity"),
    url(r'^updatecommunitystatus/$', UpdateCommunityStatus.as_view(), name="updateCommunityStatus"),
    url(r'^getactivecommunity/$', GetActiveCommunity.as_view(), name="getAllCommunity"),


    #users
    url(r'^insertuser/$', InsertUser.as_view(), name="insertUser"),
    url(r'^getuserbyid/$', GetUserById.as_view(), name="getUserById"),
    url(r'^getalluser/$', GetAllUser.as_view(), name="getAllUser"),
    url(r'^getcommunity/$', GetCommunity.as_view(), name="getCommunity"),
    url(r'^userlogin/$', UserLogin.as_view(), name="userLogin"),
    url(r'^changeuserpassword/$', ChangeUserPassword.as_view(), name="changeUserPassword"),
    url(r'^forgetuserpassword/$', ForgetUserPassword.as_view(), name="forgetUserPassword"),
    url(r'^updateuserdetails/$', UpdateUserDetails.as_view(), name="updateUserDetails"),
    url(r'^updateuserstatus/$', UpdateUserStatus.as_view(), name="updateUserStatus"),
    url(r'^updateusrprofile/$', UpdateUserprofile.as_view(), name="updateusrprofile"),
    #units
    url(r'^insertunit/$', InsertUnit.as_view(), name="insertUnit"),
    url(r'^getallunit/$', GetAllUnit.as_view(), name="getAllUnit"),
    url(r'^updateunit/$', UpdateUnit.as_view(), name="updateUnit"),

    #brands
    url(r'^insertbrand/$', InsertBrand.as_view(), name="insertBrand"),
    url(r'^getallbrand/$', GetAllBrand.as_view(), name="getAllBrand"),
    url(r'^getbrandbyid/$', GetBrandById.as_view(), name="getBrandById"),
    url(r'^updatebrandname/$', UpdateBrandName.as_view(), name="updateBrandName"),
    url(r'^updatebrandstatus/$', UpdateBrandStatus.as_view(), name="updateBrandStatus"),

    #categories
    url(r'^insertcategory/$', InsertCategory.as_view(), name="insertCategory"),
    url(r'^getallcategory/$', GetAllCategory.as_view(), name="getAllCategory"),
    url(r'^getcategorybyid/$', GetCategoryById.as_view(), name="getCategoryById"),
    url(r'^updatecategoryname/$', UpdateCategoryName.as_view(), name="updateCategoryName"),
    url(r'^updatecategorystatus/$', UpdateCategoryStatus.as_view(), name="updateCategoryStatus"),

    #product_catalog
    url(r'^insertproduct/$', InsertProduct.as_view(), name="insertProduct"),
    url(r'^getallproducts/$', GetAllProducts.as_view(), name="getAllProducts"),
    url(r'^getActiveCategory/$', GetActiveCategory.as_view(), name="getActiveCategory"),

    url(r'^getproductbyid/$', GetProductById.as_view(), name="getProductById"),
    url(r'^updateproductdetails/$', UpdateProductDetails.as_view(), name="updateProductDetails"),
    url(r'^updateproductstatus/$', UpdateProductStatus.as_view(), name="updateProductStatus"),
    url(r'^getproductbycategory/$', GetProductByCategory.as_view(), name="getProductByCategory"),
    url(r'^getproductbybrand/$', GetProductByBrand.as_view(), name="getProductByBrand"),
    url(r'^adminproducts/$', AdminGetAllProducts.as_view(), name="AdminGetAllProducts"),

    #user_cart
    url(r'^insertusercart/$', InsertUserCart.as_view(), name="insertUserCart"),
    url(r'^getusercart/$', GetUserCart.as_view(), name="getUserCart"),
    url(r'^updatequantity/$', UpdateQuantity.as_view(), name="updateQuantity"),
    url(r'^deletefromcart/$', DeleteFromCart.as_view(), name="deleteFromCart"),

	# STORE IMAGES
    # url( 'insertimg/', Prodimageupload.as_view(), name="addimage" ),
    # url( 'cmtinsertimg/', PortalCmtimageupload.as_view(), name="portalCmtimageupload" ),

    # IMAGE UPLOADING
    url(r'^insertimg/$', Prodimageupload.as_view(), name="Prodimageupload"),
    url(r'^cmtinsertimg/$', PortalCmtimageupload.as_view(), name="PortalCmtimageupload"),
    url(r'^profinsertimg/$', Profileimageupload.as_view(), name="profinsertimg"),


    #user_orders
    url(r'^insertuserorder/$', InsertUserOrder.as_view(), name="insertUserOrder"),
    url(r'^getalluserorder/$', GetAllUserOrder.as_view(), name="getAllUserOrder"),
    url(r'^getuserorderbyid/$', GetUserOrderById.as_view(), name="getUserOrderById"),
    url(r'^getuserorderdetailbyid/$', GetUserOrderDetailById.as_view(), name="getUserOrderDetailById"),
    url(r'^updateorderstatus/$', UpdateOrderStatus.as_view(), name="updateOrderStatus"),
    url(r'^getordcountbyordid/$', GetUserOrdCntbyordId.as_view(), name="getordcountbyordid"),


    #vendor_prod_mapping
    url(r'^insertvendorprodmap/$', InsertVendorProdMap.as_view(), name="insertVendorProdMap"),
    url(r'^getvendorprodmap/$', GetVendorProdMap.as_view(), name="getVendorProdMap"),
    url(r'^deletevendorprod/$', DeleteVendorProduct.as_view(), name="getVendorProdMap"),

    #portal_admin
    url(r'^insertportaladmin/$', InsertPortalAdmin.as_view(), name="insertPortalAdmin"),
    url(r'^getportaladminbyid/$', GetPortalAdminById.as_view(), name="getPortalAdminById"),
    url(r'^portaladminlogin/$', PortalAdminLogin.as_view(), name="portalAdminLogin"),
    url(r'^changeportaladminpassword/$', ChangePortalAdminPassword.as_view(), name="changePortalAdminPassword"),
    url(r'^forgetportaladminpassword/$', ForgetPortalAdminPassword.as_view(), name="forgetPortalAdminPassword"),
    url(r'^updateportaladmindetails/$', UpdatePortalAdminDetails.as_view(), name="updatePortalAdminDetails"),
      url(r'^upprtladminprofile/$', UpdatePortalAdminProfile.as_view(), name="upprtladminprofile"),

    #issue tracker
    url(r'^insertusrissue/$', InsertUsrIssue.as_view(), name="insertUserIssue"),

    #order_grouping
    url(r'^getoverallorders/$', GetOverallOrders.as_view(), name="ordergrouping"),
    url(r'^getordersbyProdid/$', GetOrdersByprodId.as_view(), name="ordergrouping"),
    url(r'^getvndsbyProdid/$', GetVndByProdId.as_view(), name="ordergrouping"),
    url( r'^getvndPrice/$', VendorprodPrice.as_view(), name="getvndPrice" ),


    # assigning product to vendors
    url(r'^orderprodvndassign/$', InsertVndAssignProducts.as_view(), name="assignprodtovendor"),

    url( r'^acceptedvendororders/$', GetAcptVndOrdersByIdCnt.as_view(), name="acceptedvendororders" ),
    url( r'^pendingvendororders/$', GetPndVndOrdersByIdCnt.as_view(), name="pendingvendororders" ),
    url( r'^shippedvendororders/$', GetShpVndOrdersByIdCnt.as_view(), name="shippedvendororders" ),
    url( r'^vendorshpordersbyuserId/$', GetVndShpOrdersByUserId.as_view(), name="vendorshpordersbyuserId" ),
    url( r'^vendordelvordersbyuserId/$', DeliveredVendorOrders.as_view(), name="vendordelvordersbyuserId" ),



    url(r'^vendorpndordersbyuserId/$', GetVndPndOrdersByUserId.as_view(), name="vendorpndordersbyuserId"),
    url( r'^vendoracptordersbyuserId/$', GetVndAcptOrdersByUserId.as_view(), name="vendoracptordersbyuserId" ),
    url(r'^dispatchorders/$', Dispatchuserorsers.as_view(), name="dispatchorders"),
    url(r'^vnddispatchorders/$', GetVndDispatchedOrdersById.as_view(), name="vnddispatchorders"),

    url( r'^vnddispatchorderdetails/$', GetVndDispatchedOrdersDetail.as_view(), name="vnddispatchorderdetails" ),
    url( r'^cancordrs/$', CancelVendorOrders.as_view(), name="cancordrs" ),
    url( r'^acceptcordrs/$', AcceptVendorOrders.as_view(), name="acceptcordrs" ),


    #ORDER TRACKING
    url(r'^getordertrackingdatabyuserid/$', GerOrdersByOrderTracking.as_view(), name="GerOrdersByOrderTracking"),
    #PRODUCTS ORDER TRACKING BY ORDER ID
    url(r'^getproductsordertrackingbyorderid/$', GetProductByOrderTracking.as_view(), name="GetProductByOrderTracking"),
    url(r'^getuserordercancellation/$', GetUserOrdersCancellationStatus.as_view(), name="GetUserOrdersCancellationStatus"),

   #GET USER ORDERS COUNT
   url(r'^getuserorderscount/$', GetUserOrdersCount.as_view(), name="GetUserOrdersCount"),


   #USER ORDERS COUNT OPEN STATUS
   url(r'^getuserorderscountforopenstatus/$', GetUserOrdersCountForOpenStatus.as_view(), name="GetUserOrdersCountForOpenStatus"),

   #USER ORDERS COUNT INPROGRESS STATUS
   url(r'^getuserorderscountforinprogressstatus/$', GetUserOrdersCountForInProgressStatus.as_view(), name="GetUserOrdersCountForInProgressStatus"),

   #USER ORDERS COUNT CLOSE STATUS
   url(r'^getuserorderscountforclosestatus/$', GetUserOrdersCountForCloseStatus.as_view(), name="GetUserOrdersCountForCloseStatus"),

   #USER TOTAL PRODUCTS COUNT
   url(r'^getusertotalproductscount/$', GetUserTotalProductsCount.as_view(), name="GetUserTotalProductsCount"),

    # VENDOR ORDERS COUNT
   url(r'^getvendorsorderscount/$', GetVendorsOrdersCount.as_view(), name="GetVendorsOrdersCount"),

    # VENDOR ORDERS COUNT
    url(r'^getvendorsordersproductcount/$', GetVendorsOrdersProductCount.as_view(), name="GetVendorsOrdersProductCount"),

    # VENDOR ORDERS COUNT
    url(r'^getvendorassignproductcount/$', GetVendorsAssignProductCount.as_view(), name="GetVendorsAssignProductCount"),

    # VENDOR ORDERS COUNT
    url(r'^getvendorshippedproductcount/$', GetVendorShippedProductCount.as_view(), name="GetVendorShippedProductCount"),

    # PAYMENTS
    url(r'^getallpayments/$', GetAllPayments.as_view(),name="GetAllPayments"),
    url(r'^getbalanceamt/$', GetBalanceAmount.as_view(),name="Getbalanceamt"),


    #PAYMENT DETAILS
    url(r'^getpaymentdetailsbyid/$', GetAllPaymentsDetails.as_view(),name="GetAllPaymentsDetails"),

   #INSERT PAYMENT
    url(r'^insertpayments/$', InsertPayments.as_view(),name="InsertPayments"),
   #INSERT ADMIN PAYMENTS
     url(r'^insertadminpayments/$', InsertAdminPayments.as_view(),name="InsertAdminPayments"),

# vendor order counts Vendorpendingcount
    url(r'^vendorpendingcount/$', Vendorpendingcount.as_view(),name="Vendorpendingcount"),

    # ADMIN PAYMENTS
       url(r'^getadminpayments/$', GetAminPayments.as_view(),name="GetAminPayments"),

    # PORTAL ORDERS
    url( r'^getprtlpndordbydt/$', GetPrtlorders.as_view(), name="getprtlpndordbydt" ),
    url( r'^getprtlorddetbyusr/$', Getprtlorderdetails.as_view(), name="getprtlorddetbyusr" ),
    url( r'^getprtlusrord/$', GetPrtlUsrorders.as_view(), name="getprtlusrord" ),
    url( r'^getprtlusrdet/$', GetPrtlUsrorddet.as_view(), name="getprtlusrdet" ),





]