from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from django.db import connection
# from lib.logger import logger
from lib.logger.logger import logger
from lib.logger import logmodules
from api.vendor_prod_assign import vendor_prod_assign_querries
from lib.query_executer.queryExecuter import QueryExecute

class InsertVndAssignProducts(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger(logmodules.VENDOR_PRODUCT_ASSIGN).get()
        self.error_logger = logger(logmodules.VENDOR_PRODUCT_ASSIGN_ERROR).get()
        self.logger.info("VENDOR_PRODUCT_ASSIGN API Service Started")

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 30 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            cursor = connection.cursor()
            OrdData = request.data['OrdData']
            CrtBy =request.data['createdBy']
            UpBy = request.data['updateBy']
            for x in OrdData :
                print(x)
                 # UPDATING ORDER DETAIL STATUS
                args =[x['order_id'],x['order_detail_id']]
                result = QueryExecute.insert_execute_many( args, vendor_prod_assign_querries.UPDATE_ORDER_DETAIL_STATUS,cursor )
                 # UPDATING ORDER STATUS
                args = [x['order_id']]
                result1 = QueryExecute.insert_execute_many( args, vendor_prod_assign_querries.UPDATE_USER_ORDERS,cursor )

            #  INSERT DATA INTO order_prod_group_vendor_assign
                args = [x['product_id'],x['quantity'],x['vendor_id'],CrtBy,UpBy]
                result = QueryExecute.insert_execute_many( args, vendor_prod_assign_querries.INSERT_ASSIGNED_PRODUCTS_TO_VND,cursor )
            #  INSERT DATA INTO order_prod_group_vendor_details
                order_prod_grouping_id = QueryExecute.execute_many( [], vendor_prod_assign_querries.GET_LAST_VALUE, cursor )
            # for orderDetId in OrdData:
                OrdDetailId = x['order_detail_id']
                args = [order_prod_grouping_id,OrdDetailId,CrtBy,UpBy]
                Finalresult = QueryExecute.insert_execute_many( args,vendor_prod_assign_querries.order_prod_group_vendor_details,cursor )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            cursor.close()
            print( Finalresult )
            return HttpResponse( json.dumps(QueryExecute.result_insert) )




class GetPndVndOrdersByIdCnt(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            VndId = request.data['vendor_id']
            result = QueryExecute.get_json( [VndId], vendor_prod_assign_querries.GET_VENDOR_ORDERS_PENDING_COUNT_BY_USER_NAME )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class GetShpVndOrdersByIdCnt( generics.GenericAPIView ):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self, request):
        try:
            VndId = request.data['vendor_id']
            result = QueryExecute.get_json( [VndId],vendor_prod_assign_querries.GET_VENDOR_PRODUCT_SHIPPED_COUNT_BY_USER_NAME )
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )
        else:
            print( result )
            return HttpResponse( json.dumps( result ) )


class GetAcptVndOrdersByIdCnt(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            VndId = request.data['vendor_id']
            result = QueryExecute.get_json( [VndId], vendor_prod_assign_querries.GET_VENDOR_PRODUCT_ACCEPTED_COUNT_BY_USER_NAME )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class GetVndPndOrdersByUserId(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:

            VndId = request.data['vendor_id']
            UsrId = request.data['user_id']
            args = [VndId,UsrId]
            result = QueryExecute.get_json( args, vendor_prod_assign_querries.GET_VENDOR_PRODUCT_ORDERS_PENDING_BY_USER_NAME )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class GetVndAcptOrdersByUserId(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:

            VndId = request.data['vendor_id']
            UsrId = request.data['user_id']
            args = [VndId,UsrId]
            result = QueryExecute.get_json( args, vendor_prod_assign_querries.GET_VENDOR_PRODUCT_ORDERS_ACCEPTED_BY_USER_NAME )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class Dispatchuserorsers(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

    # Method Name                : post
    # Description                : Insert products assigned to Vendor
    # Created By                 : Harish
    # Created Date               : 31 May 2019
    # Last Modified By           :
    # Last Modified Date         :
    # Modification Description   :
    def post(self,request):
        try:
            cursor = connection.cursor()
            usrId = request.data['order_detail_id']
            grpId = request.data['order_prod_grouping_id']
            args =[  grpId]
            result = QueryExecute.insert_execute_many( args, vendor_prod_assign_querries.UPDATE_VENDOR_ORDER_STATUS_AS_CLOSED, cursor )
            args1 =[ usrId]
            result1 = QueryExecute.insert_execute_many( args1, vendor_prod_assign_querries.DISPATCH_USER_PRODUCTS,cursor )
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )
        else:
            return HttpResponse( json.dumps(QueryExecute.result_update ))

class GetVndDispatchedOrdersById(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            VndId = request.data['vendor_id']
            result = QueryExecute.get_json( [VndId], vendor_prod_assign_querries.GET_VENDOR_PRODUCT_DELIVERED_COUNT_BY_USER_NAME )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class GetVndDispatchedOrdersDetail(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 7 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            VndId = request.data['vendor_id']
            Usrid = request.data['user_id']
            result = QueryExecute.get_json( [VndId,Usrid], vendor_prod_assign_querries.GET_VENDOR_DISPATCH_ORDER_DETAILS_BY_USERID )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class CancelVendorOrders(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR_PROD_MAP).get()
        self.error_logger = logger(logmodules.VENDOR_PROD_MAP_ERROR).get()
        self.logger.info("Vendor product map API Service Started")

        # Method Name                : post
        # Description                : Return Vendor product map
        # Created By                 : Harish
        # Created Date               : 25 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            orddetId = request.data['ord_det_id']
            grpid = request.data['group_id']
            result = QueryExecute.get_json([grpid], vendor_prod_assign_querries.UPDATE_VENDOR_ORDER_STATUS)
            result1 = QueryExecute.get_json( [orddetId],vendor_prod_assign_querries.UPDATE_USER_ORDER_DETAIL_STATUS )
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class AcceptVendorOrders(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR_PROD_MAP).get()
        self.error_logger = logger(logmodules.VENDOR_PROD_MAP_ERROR).get()
        self.logger.info("Vendor product map API Service Started")

        # Method Name                : post
        # Description                : Return Vendor product map
        # Created By                 : Harish
        # Created Date               : 26
        # JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            grpId = request.data['group_id']
            orddetId = request.data['ord_det_id']
            usrId = request.data['user_id']
            ordId = request.data['order_id']
            args=[grpId]
            args1=[orddetId]
            args2 = [ordId]
            result = QueryExecute.get_json(args, vendor_prod_assign_querries.UPDATE_VENDOR_ORDER_STATUS_AS_ASSIGN)
            result1 = QueryExecute.get_json( args1,vendor_prod_assign_querries.UPDATE_USER_ORDER_DETAIL_STATUS_AS_PACKING )
            result2 = QueryExecute.get_json(args2,vendor_prod_assign_querries.UPDATE_USER_PRODUCTS_STATUS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class GetVndShpOrdersByUserId(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN ).get()
        self.error_logger = logger( logmodules.VENDOR_PRODUCT_ASSIGN_ERROR ).get()
        self.logger.info( "GET_VENDOR_ORDERS API Service Started" )

        # Method Name                : post
        # Description                : Insert products assigned to Vendor
        # Created By                 : Harish
        # Created Date               : 31 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:

            VndId = request.data['vendor_id']
            UsrId = request.data['user_id']
            args = [VndId,UsrId]
            result = QueryExecute.get_json( args, vendor_prod_assign_querries.GET_VENDOR_PRODUCT_ORDERS_SHIPPED_BY_USER_NAME )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            print( result )
            return HttpResponse( json.dumps(result))

class DeliveredVendorOrders(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.VENDOR_PROD_MAP).get()
        self.error_logger = logger(logmodules.VENDOR_PROD_MAP_ERROR).get()
        self.logger.info("Vendor product map API Service Started")

        # Method Name                : post
        # Description                : Return Vendor product map
        # Created By                 : Harish
        # Created Date               : 02 JULY
        # JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            cursor = connection.cursor()
            orddetId = request.data['ord_det_id']
            ordId = request.data['order_id']
            args1=[orddetId]
            args2 = [ordId]
            result = QueryExecute.execute_many( args2, vendor_prod_assign_querries.CHK_ALL_ITEMS_DELIVERED,cursor)
            if(result == 1):
                result1 = QueryExecute.get_json(args2, vendor_prod_assign_querries.UPDATE_USER_ORDERS_STATUS_AS_CLOSED)

            result2 = QueryExecute.get_json( args1,vendor_prod_assign_querries.UPDATE_USER_ORDER_DETAIL_STATUS_AS_DELIVERED )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:

            print(result2)
            return HttpResponse(json.dumps(QueryExecute.result_update))


class Vendorpendingcount(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger( logmodules.VENDOR_PROD_MAP ).get()
        self.error_logger = logger( logmodules.VENDOR_PROD_MAP_ERROR ).get()
        self.logger.info( "Vendor pending count API Service Started" )
        # Method Name                : post
        # Description                : Returns count of Orders pending status
        # Created By                 : Shiva
        # Created Date               : 15 JULY 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :
    def post(self,request):
        try:
            vndId = request.data['vendor_id']
            # args = [vndId]
            result = QueryExecute.get_json([vndId], vendor_prod_assign_querries.GET_VENDOR_ORDERS_PENDING)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse( json.dumps( result ) )