UPDATE_ORDER_DETAIL_STATUS = 'UPDATE user_order_details SET order_detail_status = "ASSIGNED"  WHERE order_id = %s AND order_detail_id = %s'

UPDATE_USER_ORDERS = 'UPDATE user_orders SET order_status = "In-Progress" WHERE order_id = %s'

GET_LAST_VALUE='select LAST_INSERT_ID() as id'

INSERT_ASSIGNED_PRODUCTS_TO_VND = 'INSERT INTO order_prod_group_vendor_assign(product_id,total_quantity,vendor_id,created_by,' \
                                  'created_date,updated_by,updated_date) VALUES(%s,%s,%s,%s,NOW(),%s,Now())'

order_prod_group_vendor_details = 'INSERT INTO order_prod_group_vendor_details(order_prod_grouping_id,order_detail_id,' \
                                  'created_by,created_date,updated_by,updated_date) VALUES(%s,%s,%s,NOW(),%s,Now())'

# GETTING OVERALL ORDERS BY VENDOR ID
GET_VENDOR_PRODUCT_ORDERS = 'SELECT ogvnasgn.order_prod_grouping_id,vendor_assign_detail_id,usroddet.order_detail_id,' \
                            'CONCAT(usrenrl.first_name,usrenrl.last_name) AS username,prodctg.product_name,' \
                            'CONCAT( prodctg.measurement,ut.unit_type) AS weight,usroddet.quantity,' \
                            'cast(usroddet.product_price as char) AS costperunit FROM((((((order_prod_group_vendor_details AS ogvndet ' \
                            'INNER  JOIN order_prod_group_vendor_assign AS ogvnasgn ON ogvndet.order_prod_grouping_id = ogvnasgn.order_prod_grouping_id) ' \
                            'INNER JOIN user_order_details AS usroddet ON ogvndet.order_detail_id = usroddet.order_detail_id)' \
                            'INNER JOIN product_catalog  AS prodctg ON  ogvnasgn.product_id = prodctg.product_id )' \
                            'INNER JOIN unit_types AS ut ON prodctg.unit_type_id = ut.unit_type_id ) INNER JOIN user_orders AS usrod ' \
                            'ON usroddet.order_id = usrod.order_id) INNER JOIN user_enrollment AS usrenrl' \
                            'ON  usrod.user_id =usrenrl.user_id )WHERE ogvnasgn.vendor_id = %s'

GET_VENDOR_ORDERS_PENDING_COUNT_BY_USER_NAME ='SELECT  usrod.user_id,CONCAT(usrenrl.first_name,usrenrl.last_name) AS username,usroddet.order_detail_status,' \
                                              'CAST(count(usroddet.quantity) AS CHAR) AS noofitems, CAST(sum(vnprmap.vendor_price * usroddet.quantity) AS CHAR) AS total FROM ' \
                                              ' vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ' \
                                              ' ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.vendor_id =%s AND' \
                                              ' ogvnasgb.product_id = vnprmap.product_id AND ogvnasgb.order_vendor_status="ASSIGN"' \
                                              ' INNER  JOIN order_prod_group_vendor_details AS ogvndet ON' \
                                              '  ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id INNER JOIN user_order_details AS usroddet ' \
                                              'ON ogvndet.order_detail_id = usroddet.order_detail_id AND usroddet.order_detail_status = "ASSIGNED" INNER JOIN' \
                                              ' user_orders AS usrod ON usroddet.order_id = usrod.order_id INNER JOIN user_enrollment AS usrenrl ON  ' \
                                              'usrod.user_id =usrenrl.user_id  GROUP BY usrod.user_id'

GET_VENDOR_PRODUCT_ACCEPTED_COUNT_BY_USER_NAME ='SELECT uo.user_id,CONCAT(ue.first_name,ue.last_name) as username,opgva.order_prod_grouping_id,uod.order_detail_id,' \
                                              'CAST(count(uod.quantity) AS CHAR) AS noofitems, cast(sum(opgva.total_quantity * vndprod.vendor_price) as char)as  total' \
                                              ' FROM order_prod_group_vendor_assign as opgva join order_prod_group_vendor_details' \
                                              ' as opgvd on opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id  AND opgva.order_vendor_status="ACCEPT"' \
                                              ' INNER JOIN vendor_prod_mapping AS vndprod ON vndprod.vendor_id = opgva.vendor_id AND ' \
                                              'vndprod.product_id = opgva.product_id JOIN user_order_details as uod on uod.order_detail_id = opgvd.order_detail_id ' \
                                              'JOIN user_orders as uo on uo.order_id = uod.order_id JOIN user_enrollment as ue' \
                                              ' on ue.user_id = uo.user_id WHERE opgva.vendor_id = %s AND uod.order_detail_status = "PACKING" GROUP BY uo.user_id'

GET_VENDOR_PRODUCT_SHIPPED_COUNT_BY_USER_NAME ='SELECT uo.user_id,CONCAT(ue.first_name,ue.last_name) as username,opgva.order_prod_grouping_id,uod.order_detail_id,' \
                                              'CAST(count(uod.quantity) AS CHAR) AS noofitems,cast(sum(opgva.total_quantity * vndprod.vendor_price) as char)as  total' \
                                              ' FROM order_prod_group_vendor_assign as opgva join order_prod_group_vendor_details' \
                                              ' as opgvd on opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id  AND opgva.order_vendor_status="CLOSED"' \
                                              ' INNER JOIN vendor_prod_mapping AS vndprod ON vndprod.vendor_id = opgva.vendor_id AND ' \
                                              'vndprod.product_id = opgva.product_id JOIN user_order_details as uod on uod.order_detail_id = opgvd.order_detail_id ' \
                                              'JOIN user_orders as uo on uo.order_id = uod.order_id JOIN user_enrollment as ue' \
                                              ' on ue.user_id = uo.user_id WHERE opgva.vendor_id = %s AND uod.order_detail_status = "SHIPPED' \
                                               '" GROUP BY uo.user_id'
GET_VENDOR_PRODUCT_DELIVERED_COUNT_BY_USER_NAME ='SELECT usrod.user_id,CAST(COUNT(usroddet.order_detail_id) AS CHAR) AS  nooforders,CONCAT(usrenrl.first_name,' \
                                                 'usrenrl.last_name) AS username,CAST(SUM(vnprmap.vendor_price * usroddet.quantity) AS CHAR) AS totalprice ' \
                                                 'FROM vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ' \
                                                 'ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.vendor_id =%s AND ' \
                                                 'ogvnasgb.product_id = vnprmap.product_id AND ogvnasgb.order_vendor_status="CLOSED" ' \
                                                 'INNER JOIN order_prod_group_vendor_details AS ogvndet ON ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id ' \
                                                 'INNER JOIN  user_order_details AS usroddet ON ogvndet.order_detail_id = usroddet.order_detail_id ' \
                                                 'AND usroddet.order_detail_status = "DELIVERED" INNER JOIN user_orders AS usrod ON  ' \
                                                 'usroddet.order_id = usrod.order_id INNER JOIN user_enrollment AS usrenrl ON ' \
                                                 'usrod.user_id =usrenrl.user_id  GROUP BY usrenrl.user_id'

GET_VENDOR_PRODUCT_ORDERS_PENDING_BY_USER_NAME = 'SELECT  usroddet.order_detail_id,CAST(usroddet.delivery_date AS CHAR) AS delivery_date,usrod.user_id,usroddet.order_detail_status,usrod.order_id,' \
                                         'ogvnasgb.order_prod_grouping_id,prodctg.product_name,CONCAT( prodctg.measurement,ut.unit_type) AS weight,usroddet.quantity,' \
                                         'CAST(vnprmap.vendor_price as char) AS costperunit,CAST(vnprmap.vendor_price * usroddet.quantity AS CHAR) AS totalprice ' \
                                         'FROM  vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ' \
                                         'ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.vendor_id =%s and ' \
                                         'ogvnasgb.product_id = vnprmap.product_id AND ogvnasgb.order_vendor_status="ASSIGN" INNER  JOIN order_prod_group_vendor_details AS ogvndet' \
                                         '  ON  ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id INNER JOIN ' \
                                         'user_order_details AS usroddet ON ogvndet.order_detail_id = usroddet.order_detail_id ' \
                                         'AND usroddet.order_detail_status = "ASSIGNED" INNER JOIN product_catalog  AS prodctg ' \
                                         'ON  ogvnasgb.product_id = prodctg.product_id INNER JOIN unit_types AS ut ON ' \
                                         'prodctg.unit_type_id = ut.unit_type_id INNER JOIN user_orders AS usrod ON ' \
                                         'usroddet.order_id = usrod.order_id INNER JOIN user_enrollment AS usrenrl ON ' \
                                         'usrod.user_id =usrenrl.user_id AND usrod.user_id = %s ORDER BY usroddet.delivery_date '

GET_VENDOR_PRODUCT_ORDERS_ACCEPTED_BY_USER_NAME = 'SELECT usrod.user_id,usroddet.order_detail_id,CAST(usroddet.delivery_date AS CHAR) AS delivery_date,usroddet.order_detail_status,usrod.order_id,' \
                                         'ogvnasgb.order_prod_grouping_id,prodctg.product_name,CONCAT( prodctg.measurement,ut.unit_type) AS weight,usroddet.quantity,' \
                                         'CAST(vnprmap.vendor_price as char) AS costperunit,CAST(vnprmap.vendor_price * usroddet.quantity AS CHAR) AS totalprice ' \
                                         'FROM  vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ' \
                                         'ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.vendor_id =%s and ' \
                                         'ogvnasgb.product_id = vnprmap.product_id AND ogvnasgb.order_vendor_status="ACCEPT" INNER  JOIN order_prod_group_vendor_details AS ogvndet' \
                                         '  ON  ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id INNER JOIN ' \
                                         'user_order_details AS usroddet ON ogvndet.order_detail_id = usroddet.order_detail_id ' \
                                         'AND usroddet.order_detail_status = "PACKING" INNER JOIN product_catalog  AS prodctg ' \
                                         'ON  ogvnasgb.product_id = prodctg.product_id INNER JOIN unit_types AS ut ON ' \
                                         'prodctg.unit_type_id = ut.unit_type_id INNER JOIN user_orders AS usrod ON ' \
                                         'usroddet.order_id = usrod.order_id INNER JOIN user_enrollment AS usrenrl ON ' \
                                         'usrod.user_id =usrenrl.user_id AND usrod.user_id = %s  ORDER BY usroddet.delivery_date '

GET_VENDOR_PRODUCT_ORDERS_SHIPPED_BY_USER_NAME = 'SELECT  usroddet.order_detail_id,CAST(usroddet.delivery_date AS CHAR) AS delivery_date,usrod.user_id,usroddet.order_detail_status,usrod.order_id,' \
                                         'ogvnasgb.order_prod_grouping_id,prodctg.product_name,CONCAT( prodctg.measurement,ut.unit_type) AS weight,usroddet.quantity,' \
                                         'CAST(vnprmap.vendor_price as char) AS costperunit,CAST(vnprmap.vendor_price * usroddet.quantity AS CHAR) AS totalprice ' \
                                         'FROM  vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ' \
                                         'ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.vendor_id =%s and ' \
                                         'ogvnasgb.product_id = vnprmap.product_id AND ogvnasgb.order_vendor_status="CLOSED" INNER  JOIN order_prod_group_vendor_details AS ogvndet' \
                                         '  ON  ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id INNER JOIN ' \
                                         'user_order_details AS usroddet ON ogvndet.order_detail_id = usroddet.order_detail_id ' \
                                         'AND usroddet.order_detail_status = "SHIPPED" INNER JOIN product_catalog  AS prodctg ' \
                                         'ON  ogvnasgb.product_id = prodctg.product_id INNER JOIN unit_types AS ut ON ' \
                                         'prodctg.unit_type_id = ut.unit_type_id INNER JOIN user_orders AS usrod ON ' \
                                         'usroddet.order_id = usrod.order_id INNER JOIN user_enrollment AS usrenrl ON ' \
                                         'usrod.user_id =usrenrl.user_id AND usrod.user_id = %s ORDER BY usroddet.delivery_date '

DISPATCH_USER_PRODUCTS = 'UPDATE user_order_details SET order_detail_status = "SHIPPED" WHERE order_detail_id = %s'

GET_VENDOR_DISPATCH_ORDER_DETAILS_BY_USERID = 'SELECT  usrod.order_id,prodctg.product_name,CAST(vnprmap.vendor_price AS CHAR) AS price,' \
                                              'ogvnasgb.total_quantity,CAST(ogvnasgb.created_date AS CHAR) AS crtdate, CAST(vnprmap.vendor_price * usroddet.quantity AS CHAR) AS totalprice, ' \
                                              'CONCAT(prodctg.measurement,ut.unit_type) AS weight FROM  vendor_prod_mapping AS vnprmap INNER JOIN order_prod_group_vendor_assign AS ogvnasgb ' \
                                              'ON vnprmap.vendor_id =ogvnasgb.vendor_id AND ogvnasgb.vendor_id =%s AND ' \
                                              'ogvnasgb.product_id = vnprmap.product_id AND ogvnasgb.order_vendor_status="CLOSED" ' \
                                              'INNER  JOIN order_prod_group_vendor_details AS ogvndet  ON  ' \
                                              'ogvndet.order_prod_grouping_id = ogvnasgb.order_prod_grouping_id ' \
                                              'INNER JOIN  user_order_details AS usroddet ON ogvndet.order_detail_id = usroddet.order_detail_id  ' \
                                              'AND usroddet.order_detail_status = "DELIVERED" INNER JOIN product_catalog  AS prodctg ' \
                                              'ON  ogvnasgb.product_id = prodctg.product_id INNER JOIN unit_types AS ut ' \
                                              'ON  prodctg.unit_type_id = ut.unit_type_id INNER JOIN user_orders AS usrod ' \
                                              'ON  usroddet.order_id = usrod.order_id INNER JOIN user_enrollment AS usrenrl ' \
                                              'ON usrod.user_id =usrenrl.user_id WHERE usrod.user_id = %s'

# GET_VENDOR_DISPATCH_ORDER_DETAILS = 'SELECT usroddet.order_detail_status,usroddet.order_detail_id,usrod.order_id,prodctg.product_name,' \
#                                     'CONCAT( prodctg.measurement,ut.unit_type) AS weight,usroddet.quantity,' \
#                                     'cast(usroddet.product_price as char) AS costperunit,cast(usroddet.total_price AS char )AS totalprice' \
#                                     ' FROM ((((((order_prod_group_vendor_details AS ogvndet INNER  JOIN order_prod_group_vendor_assign AS ogvnassgn' \
#                                     ' ON ogvndet.order_prod_grouping_id = ogvnassgn.order_prod_grouping_id AND ogvnassgn.order_vendor_status="CLOSED") INNER JOIN user_order_details usroddet ' \
#                                     'ON ogvndet.order_detail_id = usroddet.order_detail_id) INNER JOIN product_catalog AS prodctg ON  ' \
#                                     'ogvnassgn.product_id = prodctg.product_id ) INNER JOIN unit_types AS ut ON prodctg.unit_type_id = ut.unit_type_id )' \
#                                     ' INNER JOIN user_orders AS usrod ON usroddet.order_id = usrod.order_id) INNER JOIN user_enrollment AS usrenrl ' \
#                                     ' ON  usrod.user_id =usrenrl.user_id ) WHERE ogvnassgn.vendor_id = %s AND usrod.user_id = %s AND ' \
#                                     'usroddet.order_detail_status = "SHIPPED" AND ogvnassgn.order_prod_grouping_id = %s ORDER BY ogvnassgn.order_prod_grouping_id'




UPDATE_VENDOR_ORDER_STATUS = 'UPDATE order_prod_group_vendor_assign SET order_vendor_status = "CANCEL" WHERE order_prod_grouping_id = %s'

UPDATE_USER_ORDER_DETAIL_STATUS = 'UPDATE user_order_details SET order_detail_status = "OPEN" WHERE order_detail_id = %s'

UPDATE_VENDOR_ORDER_STATUS_AS_ASSIGN = 'UPDATE order_prod_group_vendor_assign SET order_vendor_status = "ACCEPT" WHERE order_prod_grouping_id = %s'

UPDATE_USER_ORDER_DETAIL_STATUS_AS_PACKING = 'UPDATE user_order_details SET order_detail_status = "PACKING" WHERE order_detail_id = %s'

UPDATE_VENDOR_ORDER_STATUS_AS_CLOSED = 'UPDATE order_prod_group_vendor_assign SET order_vendor_status = "CLOSED" WHERE order_prod_grouping_id = %s'

UPDATE_USER_PRODUCTS_STATUS = 'UPDATE user_orders SET products_detail_status = "ASSIGNED" WHERE order_id = %s'


UPDATE_USER_ORDER_DETAIL_STATUS_AS_DELIVERED = 'UPDATE user_order_details SET order_detail_status = "DELIVERED",payment_status = "NOT-PAID" WHERE order_detail_id = %s'

UPDATE_USER_ORDERS_STATUS_AS_CLOSED = 'UPDATE user_orders SET order_status = "CLOSED" WHERE order_id = %s'

CHK_ALL_ITEMS_DELIVERED='SELECT COUNT(order_detail_status)  FROM user_order_details  WHERE order_id = %s AND order_detail_status <> "DELIVERED"'

CHK_VENDOR_ID_IN_PAYMENT='SELECT COUNT(vendor_id)  FROM payment_settlement  WHERE vendor_id = %s'

INSERT_INTO_PAYMENT_SETTLEMENT = 'INSERT INTO payment_settlement(vendor_id,amount,payable_amount,created_by,updated_by)VALUES(%s,%s,%s,%s,%s)'

GET_PAYMENT_DATA = 'SELECT amount,balance_amount,payable_amount FROM payment_settlement WHERE vendor_id = %s'

UPDATE_PAYMENT_DETAILS ='UPDATE payment_settlement SET amount=%s,payable_amount = %s WHERE vendor_id = %s'


GET_VENDOR_ORDERS_PENDING =   'select count(opgva.order_vendor_status)as order_status_count from order_prod_group_vendor_assign as opgva inner join ' \
                              'order_prod_group_vendor_details as opgvd on opgva.order_prod_grouping_id= opgvd.order_prod_grouping_id and ' \
                              'opgva.order_vendor_status = "ASSIGN" AND opgva.vendor_id = %s inner join user_order_details as uod ' \
                              'on uod.order_detail_id= opgvd.order_detail_id and uod.order_detail_status = "ASSIGNED"'