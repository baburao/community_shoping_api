


INSERT_USERS = "Insert into user_enrollment ( first_name,last_name,phone_no,email_id,password,community_id,block_name,flat_no,"\
               "user_image,is_active,created_by,created_date,updated_by,updated_date,role) values "\
                "(%s,%s,%s,%s,%s,%s,%s,%s,'user.png','Y',%s,now(),%s,now(),'user')"

EMAIL_COUNT = "Select count(*) from user_enrollment where email_id = %s "

GET_NO_OF_BLOCK_FLAT = "Select total_blocks from community_enrolment where community_id = %s "

BLOCK_FLAT_COUNT = "Select count(*) from user_enrollment "\
                    "Where block_name = %s and flat_no = %s"

FLAT_COUNT = "Select count(flat_no) from user_enrollment "\
            "Where flat_no = %s"

GET_LAST_VALUE='select LAST_INSERT_ID() as id'

GET_USER_BY_ID = "Select user_id,first_name,last_name,ue.phone_no,ue.email_id,ue.community_id,ue.user_image,flat_no," \
                 "ue.block_name,ce.community_name  from user_enrollment as ue join community_enrolment as ce on " \
                 "ue.community_id=ce.community_id where user_id = %s"

GET_ALL_USER = "Select ue.user_id,CONCAT(first_name, ' ',last_name) as name,ce.community_name, ue.community_id,ue.email_id," \
               " ue.block_name, ue.flat_no,ue.is_active, ue.phone_no from  user_enrollment as ue join community_enrolment as ce " \
               "on ue.community_id=ce.community_id"

GET_COMMUNITY = "Select community_id,community_name from community_enrolment Where is_active = 'y'"

EMAIL_UPDATE_COUNT = "Select count(*) from user_enrollment "\
                    "Where email_id = %s and user_id != %s"

BLOCK_FLAT_UPDATE_COUNT = "Select count(*) from user_enrollment "\
                    "Where block_name = %s and flat_no = %s and user_id != %s"

FLAT_UPDATE_COUNT = "Select count(flat_no) from user_enrollment "\
                    "Where flat_no = %s and user_id != %s"

UPDATE_USER = "Update user_enrollment set first_name = %s,last_name = %s,phone_no = %s,email_id = %s, "\
              "community_id = %s,block_name = %s,flat_no = %s, updated_by = %s, updated_date = now() "\
              "where user_id = %s "
UPDATE_USER_profile = "Update user_enrollment set first_name = %s,last_name = %s,phone_no = %s, " \
                          "community_id = %s,block_name = %s,flat_no = %s,user_image = %s, updated_by = %s, updated_date = now() " \
                          "where user_id = %s "

UPDATE_USER_STATUS = "Update user_enrollment set is_active = %s "\
                    "where user_id = %s "

GET_PASSWORD = "Select password from user_enrollment where phone_no = %s"

GET_INFO_BY_USER_PWD = "Select user_id,first_name,last_name,phone_no,email_id,community_id,block_name,flat_no,role, "\
                        "user_image from user_enrollment "\
                        "Where phone_no = %s and password = %s AND  user_enrollment.is_active = 'Y'"

UPDATE_PASSWORD = "update user_enrollment set password = %s where email_id = %s"

MOBILENUM_COUNT = "SELECT Count(phone_no) FROM user_enrollment WHERE phone_no = %s"