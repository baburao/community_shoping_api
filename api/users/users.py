from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.users import users_queries
from api.vendor import vendor_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.encode.encode_decode import *
from lib.logger.logger import logger
from lib.logger import logmodules


class InsertUser(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Inserts User
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            password = request.data['password']
            community_id = request.data['community_id']
            block_name = request.data['block_name']
            flat_no = request.data['flat_no']
            # state_id = request.data['state_id']
            # city_id = request.data['city_id']
            # pincode = request.data['pincode']

            created_by = request.data['created_by']
            updated_by = request.data['updated_by']

            # result1 = QueryExecute.execute_many([email_id] ,users_queries.EMAIL_COUNT,cursor)
            # if (result1> 0):
            #     return HttpResponse(json.dumps(QueryExecute.result_duplicate))
            # result1 = QueryExecute.execute_many( [email_id], users_queries.EMAIL_COUNT, cursor )
            result1 = QueryExecute.execute_many( [phone_no], users_queries.MOBILENUM_COUNT, cursor )
            result2 = QueryExecute.execute_many([phone_no], vendor_queries.MOBILENUM_COUNT, cursor)
            if (result1 != 0 and result2 != 0):
                return HttpResponse( json.dumps("Phone_no Already Registered"))
            if(result1 == 0 and result2 != 0):
                return HttpResponse(json.dumps("Phone_no Already Registered"))
            if (result1 > 0):
                return HttpResponse( json.dumps( QueryExecute.result_duplicate ) )

            blocks  = QueryExecute.execute_many([community_id],users_queries.GET_NO_OF_BLOCK_FLAT,cursor)

            if (blocks > 1) :
                args = (block_name,flat_no)
                result1 = QueryExecute.execute_many(args, users_queries.BLOCK_FLAT_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            else :
                result1 = QueryExecute.execute_many([flat_no], users_queries.FLAT_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(json.dumps(QueryExecute.result_duplicate))

            pwds = encode(password)


            args = (first_name,last_name,phone_no,email_id,pwds,community_id,block_name,flat_no,created_by,updated_by)

            result2 = QueryExecute.insert_execute_many(args,users_queries.INSERT_USERS,cursor)

            id = QueryExecute.execute_many([], users_queries.GET_LAST_VALUE, cursor)

            result = QueryExecute.get_json_many([id], users_queries.GET_USER_BY_ID, cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))


class GetUserById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Returns User By id
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):

        try :

            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id],users_queries.GET_USER_BY_ID)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetAllUser(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Returns All User
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            query = users_queries.GET_ALL_USER
            result = QueryExecute.get_json([], query)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetCommunity(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Return Community Id and name
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([], users_queries.GET_COMMUNITY)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class UserLogin(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : User Login
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            mobileNum = request.data['mobile_num']

            result1 = QueryExecute.execute_many([mobileNum], users_queries.MOBILENUM_COUNT, cursor)
            result2 = QueryExecute.execute_many([mobileNum], vendor_queries.MOBILENUM_COUNT, cursor)

            if (result1 == 0 and result2 == 0):
                return HttpResponse(json.dumps("Invaild_UserName"))

            # elif (result1  == 0):
            #     return HttpResponse(json.dumps("Invaild_UserName"))
            elif (result1 == 1):
                password = request.data['password']
                pwd = QueryExecute.execute_many([mobileNum],users_queries.GET_PASSWORD,cursor)
                if(password == decode(pwd)):
                    args =[mobileNum,pwd]
                    result = QueryExecute.get_json_many(args,users_queries.GET_INFO_BY_USER_PWD,cursor)
                    if(len(result) == 0):
                        return HttpResponse(json.dumps("Invaild_UserName"))
                else :
                    return HttpResponse( json.dumps( "Invalid username or password.Enter correct credentials" ) )

            elif (result2 == 1):
                password = request.data['password']
                pwd = QueryExecute.execute_many([mobileNum], vendor_queries.GET_PASSWORD, cursor)
                if (password == decode(pwd)):
                    args = (mobileNum, pwd)
                    result = QueryExecute.get_json_many(args, vendor_queries.GET_INFO_BY_USER_PWD, cursor)
                    if (len(result) == 0):
                        return HttpResponse(json.dumps(("Invaild_UserName")))
                else:
                    return HttpResponse(json.dumps("Invalid username or password.Enter correct credentials"))

            else :
                return HttpResponse(json.dumps("Server Issue"))

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(result))


class ChangeUserPassword(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Change user Password
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :

            email_id = request.data['email_id']
            new_password = request.data['new_password']
            old_password = request.data['old_password']
            pwd = QueryExecute.execute_many([email_id],users_queries.GET_PASSWORD,cursor)
            print(decode(pwd))
            if (new_password == decode(pwd)):
               return HttpResponse(json.dumps("Password already exists"))
            else:
                pwds = encode(new_password)
                args = (pwds,email_id)
                result = QueryExecute.insert_execute_many(args,users_queries.UPDATE_PASSWORD,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class ForgetUserPassword(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Forget User Password
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            email_id = request.data['email_id']

            result1 = QueryExecute.execute_many([email_id], users_queries.EMAIL_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse("Invalid email id")
            elif (result1 == 1) :
                password = request.data['password']

                pwds = encode(password)
                args = (pwds,email_id)
                result = QueryExecute.insert_execute_many(args,users_queries.UPDATE_PASSWORD,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse( json.dumps( QueryExecute.result_update ) )



class UpdateUserDetails(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Updates User Details
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        cursor = connection.cursor()
        try:

            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            community_id = request.data['community_id']
            block_name = request.data['block_name']
            flat_no = request.data['flat_no']
            updated_by = request.data['updated_by']
            user_id = request.data['user_id']

            args = [email_id, user_id]
            result1 = QueryExecute.execute_many(args, users_queries.EMAIL_UPDATE_COUNT, cursor)
            if (result1 > 0):
                return HttpResponse(QueryExecute.result_duplicate)

            blocks = QueryExecute.execute_many([community_id], users_queries.GET_NO_OF_BLOCK_FLAT, cursor)

            if (blocks > 1):
                args = (block_name, flat_no, user_id)
                result1 = QueryExecute.execute_many(args, users_queries.BLOCK_FLAT_UPDATE_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(QueryExecute.result_duplicate)

            else:
                args = (flat_no, user_id)
                result1 = QueryExecute.execute_many(args, users_queries.FLAT_UPDATE_COUNT, cursor)
                if (result1 > 0):
                    return HttpResponse(QueryExecute.result_duplicate)

            args = (first_name, last_name, phone_no, email_id, community_id, block_name, flat_no, updated_by, user_id)

            result = QueryExecute.insert_execute_many(args, users_queries.UPDATE_USER, cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))


class UpdateUserStatus(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Updates User Status
        # Created By                 : Satwika
        # Created Date               : 29 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            is_active = request.data['is_active']
            user_id = request.data['user_id']

            args = [is_active,user_id]
            result = QueryExecute.insert_execute(args,users_queries.UPDATE_USER_STATUS)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class UpdateUserprofile(generics.GenericAPIView):
    def __init__(self):
        self.logger = logger(logmodules.USER).get()
        self.error_logger = logger(logmodules.USER_ERROR).get()
        self.logger.info("User API Service Started")

        # Method Name                : post
        # Description                : Updates User Profile
        # Created By                 : Harish
        # Created Date               : 18 june 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        cursor = connection.cursor()
        try:
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            community_id = request.data['community_id']
            block_name = request.data['block_name']
            flat_no = request.data['flat_no']
            updated_by = request.data['updated_by']
            user_id = request.data['user_id']
            upImgName = request.data['upImgName']

            blocks = QueryExecute.execute_many( [community_id], users_queries.GET_NO_OF_BLOCK_FLAT, cursor )

            if (blocks > 1):
                args = (block_name, flat_no, user_id)
                result1 = QueryExecute.execute_many( args, users_queries.BLOCK_FLAT_UPDATE_COUNT, cursor )
                if (result1 > 0):
                    return HttpResponse( QueryExecute.result_duplicate )
            else:
                args = (flat_no, user_id)
                result1 = QueryExecute.execute_many( args, users_queries.FLAT_UPDATE_COUNT, cursor )
                if (result1 > 0):
                    return HttpResponse( QueryExecute.result_duplicate )

            args = (first_name, last_name, phone_no, community_id, block_name, flat_no,upImgName, updated_by, user_id)

            result = QueryExecute.insert_execute_many( args, users_queries.UPDATE_USER_profile, cursor )

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error( http_err )
            return HttpResponse( err )

        else:
            cursor.close()
            print( result )
            return HttpResponse( json.dumps( QueryExecute.result_update ) )
