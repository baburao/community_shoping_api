from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.state import state_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertState(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.STATE).get()
        self.error_logger = logger(logmodules.STATE_ERROR).get()
        self.logger.info("State API Service Started")

        # Method Name                : post
        # Description                : Inserts State
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            state_name = request.data['state_name']
            result1 = QueryExecute.execute_many([state_name],state_queries.STATE_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(QueryExecute.result_duplicate)

            result = QueryExecute.insert_execute_many([state_name],state_queries.INSERT_STATE,cursor)


        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_insert))

class GetAllState(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.STATE).get()
        self.error_logger = logger(logmodules.STATE_ERROR).get()
        self.logger.info("State API Service Started")

        # Method Name                : post
        # Description                : Returns All State
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            result = QueryExecute.get_json([],state_queries.GET_ALL_STATE)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class GetStateById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.STATE).get()
        self.error_logger = logger(logmodules.STATE_ERROR).get()
        self.logger.info("State API Service Started")

        # Method Name                : post
        # Description                : Return State by Id
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        try :
            state_id = request.data['state_id']

            result = QueryExecute.get_json([state_id],state_queries.GET_STATENAME_BY_ID)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse(json.dumps(result))

class UpdateStateName(generics.GenericAPIView) :

    def __init__(self):
        self.logger = logger(logmodules.STATE).get()
        self.error_logger = logger(logmodules.STATE_ERROR).get()
        self.logger.info("State API Service Started")

        # Method Name                : post
        # Description                : Update State Name
        # Created By                 : Satwika
        # Created Date               : 28 Apr 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            state_id = request.data['state_id']
            state_name = request.data['state_name']

            args = [state_name]
            result1 = QueryExecute.execute_many(args,state_queries.STATE_COUNT,cursor)

            if (result1 > 0):
                return HttpResponse(QueryExecute.result_duplicate)

            args  = [state_name,state_id]
            result = QueryExecute.insert_execute_many(args,state_queries.UPDATE_STATENAME,cursor)

        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

