
INSERT_STATE='insert into states (state_name) values (%s)'

STATE_COUNT = 'Select count(state_name) as cnt from states where state_name = %s'

GET_STATENAME_BY_ID = 'Select state_name from states Where state_id = %s'

UPDATE_STATENAME = 'Update states set state_name = %s Where state_id = %s'

GET_ALL_STATE = 'Select state_id,state_name from states'