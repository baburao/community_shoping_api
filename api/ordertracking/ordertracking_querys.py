
# Description                : ORDER TRACKING QUERYS
# Created By                 : BABURAO M
# Created Date               : 01 JUNE 2019

GET_ORDERS_BY_USER_ID = "select CAST(order_id AS CHAR) AS order_id,order_date,CAST(total_products AS CHAR) AS total_products,cast(total_amount as CHAR) AS amount,transaction_status," \
                         "order_status,products_detail_status from user_orders where user_id=%s group by order_id desc"

GET_ORDERS_BY_ORDER_ID = "SELECT CONCAT(ue.first_name,ue.last_name) AS username,uod.order_detail_id,uod.product_id,prodctg.product_name," \
                         "b.brand_name,CONCAT(prodctg.measurement,ut.unit_type) AS weight,uod.quantity,CAST(uod.product_price AS CHAR) AS price," \
                         "CAST(uod.total_price AS CHAR) AS total,CAST(uod.delivery_date AS CHAR) AS delivery_date,CAST(uod.delivery_time AS CHAR) AS delivery_time," \
                         "uod.order_detail_status FROM user_enrollment AS ue INNER JOIN user_orders AS uo ON uo.user_id = ue.user_id  " \
                         "INNER JOIN user_order_details AS uod ON uo.order_id = uod.order_id INNER JOIN product_catalog AS prodctg ON " \
                         "prodctg.product_id = uod.product_id INNER JOIN unit_types AS ut  ON ut.unit_type_id = prodctg.unit_type_id " \
                         "INNER JOIN brands AS b ON b.brand_id = prodctg.brand_id WHERE  uo.order_id = %s"

USER_ORDERS_COUNT = 'select count(order_id) as total_orders  from user_orders as uo where uo.user_id = %s'

USER_ORDERS_COUNT_OPEN_STATUS = 'select count(order_id) as open_orders from user_orders as uo WHERE uo.order_status = "OPEN" and uo.user_id = %s'

USER_ORDERS_COUNT_INPROGRESS_STATUS = 'select count(order_id) as inprogress_orders from user_orders as uo WHERE uo.order_status = "In-Progress" and uo.user_id =  %s'

USER_ORDERS_COUNT_CLOSE_STATUS = "select count(order_id) as close_orders from user_orders WHERE order_status = 'CLOSE'"

USER_TOTAL_PRODUCTS_COUNT = 'select count(order_id) as inprogress_orders from user_orders as uo WHERE uo.order_status = "In-Progress" and uo.user_id = 1 = %s'

VENDOR_ORDERS_COUNT = 'select count(uo.order_id) as ordercount from order_prod_group_vendor_assign as opgva join ' \
                      'order_prod_group_vendor_details as opgvd on opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id' \
                      ' join user_order_details as uod on uod.order_detail_id = opgvd.order_detail_id join user_orders as uo on' \
                      ' uo.order_id = uod.order_id where opgva.vendor_id = %s'

VENDOR_ORDERS_PRODUCT_COUNT = 'select count(opgvd.order_detail_id) as orderproduct from order_prod_group_vendor_assign as opgva join ' \
                      'order_prod_group_vendor_details as opgvd on opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id' \
                      ' join user_order_details as uod on uod.order_detail_id = opgvd.order_detail_id join user_orders as uo on' \
                      ' uo.order_id = uod.order_id where opgva.vendor_id = %s'

VENDOR_ASSIGN_PRODUCT_COUNT = 'select count(opgvd.order_detail_id) as assignproduct from order_prod_group_vendor_assign as opgva join ' \
                      'order_prod_group_vendor_details as opgvd on opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id' \
                      ' join user_order_details as uod on uod.order_detail_id = opgvd.order_detail_id join user_orders as uo on' \
                      ' uo.order_id = uod.order_id where opgva.vendor_id = %s and uod.order_detail_status = "ASSIGNED" '

VENDOR_SHIPPED_PRODUCT_COUNT = 'select count(opgvd.order_detail_id) as shippedproduct from order_prod_group_vendor_assign as opgva join ' \
                      'order_prod_group_vendor_details as opgvd on opgva.order_prod_grouping_id = opgvd.order_prod_grouping_id' \
                      ' join user_order_details as uod on uod.order_detail_id = opgvd.order_detail_id join user_orders as uo on' \
                      ' uo.order_id = uod.order_id where opgva.vendor_id = %s and uod.order_detail_status = "SHIPPED" '

CANCEL_ORDERID_USERORDER = 'update user_orders as uo set uo.order_status="CANCELLED",uo.products_detail_status="CANCELLED" ' \
                           'where  uo.order_id = %s'

CANCEL_ORDERID_USERDETAIL = 'update user_order_details as uod set uod.order_detail_status ="CANCELLED" where  ' \
                            'uod.order_id = %s'