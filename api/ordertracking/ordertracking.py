
from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.ordertracking import ordertracking_querys
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.logger.logger import logger
from lib.logger import logmodules
import datetime

class GerOrdersByOrderTracking( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], ordertracking_querys.GET_ORDERS_BY_USER_ID)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

class GetProductByOrderTracking( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            order_id = request.data['order_id']
            result = QueryExecute.get_json([order_id], ordertracking_querys.GET_ORDERS_BY_ORDER_ID)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

#GET USER ORDERS COUNT
class GetUserOrdersCount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], ordertracking_querys.USER_ORDERS_COUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

#USER ORDERS COUNT OPEN STATUS
class GetUserOrdersCountForOpenStatus( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], ordertracking_querys.USER_ORDERS_COUNT_OPEN_STATUS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))


#USER ORDERS COUNT INPROGRESS STATUS
class GetUserOrdersCountForInProgressStatus( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], ordertracking_querys.USER_ORDERS_COUNT_INPROGRESS_STATUS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))


#USER ORDERS COUNT CLOSE STATUS
class GetUserOrdersCountForCloseStatus( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], ordertracking_querys.USER_ORDERS_COUNT_INPROGRESS_STATUS)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

#USER TOTAL PRODUCTS COUNT
class GetUserTotalProductsCount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            user_id = request.data['user_id']
            result = QueryExecute.get_json([user_id], ordertracking_querys.USER_TOTAL_PRODUCTS_COUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

#VENDORS ORDERS COUNT
class GetVendorsOrdersCount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            vendor_id = request.data['vendor_id']
            result = QueryExecute.get_json([vendor_id], ordertracking_querys.VENDOR_ORDERS_COUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))


#VENDORS ORDER PRODUCT COUNT
class GetVendorsOrdersProductCount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            vendor_id = request.data['vendor_id']
            result = QueryExecute.get_json([vendor_id], ordertracking_querys.VENDOR_ORDERS_PRODUCT_COUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

#VENDORS ASSIGN PRODUCT COUNT
class GetVendorsAssignProductCount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            vendor_id = request.data['vendor_id']
            result = QueryExecute.get_json([vendor_id], ordertracking_querys.VENDOR_ASSIGN_PRODUCT_COUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

#VENDORS ASSIGN PRODUCT COUNT
class GetVendorShippedProductCount( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : GET USER ORDERS BY USER ID
        # Created By                 : BABURAO M
        # Created Date               : 01 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            vendor_id = request.data['vendor_id']
            result = QueryExecute.get_json([vendor_id], ordertracking_querys.VENDOR_SHIPPED_PRODUCT_COUNT)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            print(result)
            return HttpResponse(json.dumps(result,default=myconverter))

def myconverter(o):
    if isinstance(o, datetime.date):
        return o.__str__()





# CANCEL USER ORDER
class GetUserOrdersCancellationStatus( generics.GenericAPIView ):

    def __init__(self):
        self.logger = logger(logmodules.ORDER_TRACKING).get()
        self.error_logger = logger(logmodules.ORDER_TRACKING_ERROR).get()
        self.logger.info("Order Tracking Api Service")

        # Method Name                : post
        # Description                : CANCEL USER ORDER ID
        # Created By                 : SHIVA K
        # Created Date               : 24 JUNE 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:
            order_id = request.data['order_id']
            result = QueryExecute.get_json([order_id], ordertracking_querys.CANCEL_ORDERID_USERORDER)
            result2 = QueryExecute.get_json([order_id], ordertracking_querys.CANCEL_ORDERID_USERDETAIL)
        except Exception as err:
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            # print(result)
            return HttpResponse(json.dumps(QueryExecute.result_cancelled,default=myconverter))