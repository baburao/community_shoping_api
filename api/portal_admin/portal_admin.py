from django.http import HttpResponse
from rest_framework import generics
from rest_framework.utils import json
import json
import traceback
from api.portal_admin import portal_admin_queries
from lib.query_executer.queryExecuter import *
from django.db import connection
from lib.encode.encode_decode import *
from lib.logger.logger import logger
from lib.logger import logmodules

class InsertPortalAdmin(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Inserts Portal Admin
        # Created By                 : Satwika
        # Created Date               : 16 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor =  connection.cursor()
        try :
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            password = request.data['password']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pan_no = request.data['pan_no']
            created_by = request.data['created_by']
            updated_by = request.data['updated_by']


            args = [email_id,pan_no]
            result1 = QueryExecute.execute_many(args,portal_admin_queries.PORTAL_ADMIN_INSERT_COUNT,cursor)
            if (result1> 0):
                return HttpResponse(QueryExecute.result_duplicate)

            pwds = encode(password)

            args = [first_name,last_name,phone_no,email_id,pwds,address,state_id,city_id,pincode,pan_no,created_by,updated_by]
            result2 = QueryExecute.insert_execute_many(args,portal_admin_queries.INSERT_PORTAL_ADMIN,cursor)

            res = QueryExecute.execute_many([], portal_admin_queries.GET_LAST_VALUE, cursor)

            result = QueryExecute.get_json_many([res],portal_admin_queries.GET_PORTAL_ADMIN_BY_ID,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse((json.dumps(result)))

class GetPortalAdminById(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Returns Portal Admin by Id
        # Created By                 : Satwika
        # Created Date               : 16 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self, request):
        try:

            admin_id = request.data['admin_id']
            result = QueryExecute.get_json([admin_id], portal_admin_queries.GET_PORTAL_ADMIN_BY_ID)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            print(result)
            return HttpResponse((json.dumps(result)))

class PortalAdminLogin(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Portal Admin Login
        # Created By                 : Satwika
        # Created Date               : 16 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :

            mobile_num = request.data['mobile_num']
            password = request.data['password']
            result1 = QueryExecute.execute_many([mobile_num], portal_admin_queries.MOBILENUM_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse(json.dumps("Invaild_credentials"))
            elif (result1 == 1):

                pwd = QueryExecute.execute_many([mobile_num],portal_admin_queries.GET_PASSWORD,cursor)
                if(password == decode(pwd)):
                    args =(mobile_num,pwd)
                    result = QueryExecute.get_json_many(args,portal_admin_queries.GET_INFO_BY_USER_PWD,cursor)
                else :
                    return HttpResponse(json.dumps("Invaild_credentials"))
            else :
                return HttpResponse(json.dumps("Server Issue"))

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(result))

class ChangePortalAdminPassword(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Change portal admin password
        # Created By                 : Satwika
        # Created Date               : 16 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            email_id = request.data['email_id']

            result1 = QueryExecute.execute_many([email_id], portal_admin_queries.EMAIL_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse("Invaild UserName")
            elif (result1 == 1):
                old_password = request.data['old_password']
                pwd = QueryExecute.execute_many([email_id],portal_admin_queries.GET_PASSWORD,cursor)

                if (old_password != decode(pwd)):
                    return HttpResponse("Incorrect current password")

                new_password = request.data['new_password']

                pwds = encode(new_password)

                args = (pwds,email_id)
                result = QueryExecute.insert_execute_many(args,portal_admin_queries.UPDATE_PASSWORD,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class ForgetPortalAdminPassword(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Forgets portal admin password
        # Created By                 : Satwika
        # Created Date               : 16 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            email_id = request.data['email_id']

            result1 = QueryExecute.execute_many([email_id], portal_admin_queries.EMAIL_COUNT, cursor)
            if (result1 == 0):
                return HttpResponse("Invaild UserName")
            elif (result1 == 1):
                password = request.data['password']
                pwds = encode(password)
                args = (pwds,email_id)
                result = QueryExecute.insert_execute_many(args,portal_admin_queries.UPDATE_PASSWORD,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)
        else:
            cursor.close()
            print(result)
            return HttpResponse(QueryExecute.result_update)

class UpdatePortalAdminDetails(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Update Portal admin Details
        # Created By                 : Satwika
        # Created Date               : 16 May 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            email_id = request.data['email_id']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            pan_no = request.data['pan_no']
            updated_by = request.data['updated_by']
            admin_id = request.data['admin_id']


            args = (pan_no, email_id,admin_id)
            result1 = QueryExecute.execute_many(args,portal_admin_queries.PORTAL_ADMIN_UPDATE_COUNT,cursor)
            if (result1> 0):
                return HttpResponse(QueryExecute.result_duplicate)

            args = [first_name,last_name,phone_no,email_id,address,state_id,city_id,pincode,pan_no,updated_by,admin_id]

            result = QueryExecute.insert_execute_many(args,portal_admin_queries.UPDATE_PORTAL_ADMIN_DETAILS,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))

class UpdatePortalAdminProfile(generics.GenericAPIView):

    def __init__(self):
        self.logger = logger(logmodules.PORTAL_ADMIN).get()
        self.error_logger = logger(logmodules.PORTAL_ADMIN_ERROR).get()
        self.logger.info("Portal admin API Service Started")

        # Method Name                : post
        # Description                : Update Portal admin Details
        # Created By                 : Harish
        # Created Date               : 19 june 2019
        # Last Modified By           :
        # Last Modified Date         :
        # Modification Description   :

    def post(self,request):
        cursor = connection.cursor()
        try :
            first_name = request.data['first_name']
            last_name = request.data['last_name']
            phone_no = request.data['phone_no']
            address = request.data['address']
            state_id = request.data['state_id']
            city_id = request.data['city_id']
            pincode = request.data['pincode']
            updated_by = request.data['updated_by']
            admin_id = request.data['admin_id']
            prtlImg = request.data['prtlImg']

            args = [first_name,last_name,phone_no,address,state_id,city_id,pincode,prtlImg,updated_by,admin_id]
            result = QueryExecute.insert_execute_many(args,portal_admin_queries.UPDATE_PORTAL_ADMIN_PROFILE,cursor)

        except Exception as err :
            http_err = traceback.format_exc()
            self.error_logger.error(http_err)
            return HttpResponse(err)

        else:
            cursor.close()
            print(result)
            return HttpResponse(json.dumps(QueryExecute.result_update))