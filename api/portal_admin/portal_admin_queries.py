


INSERT_PORTAL_ADMIN = "Insert into portal_admin (first_name,last_name,phone_no,email_id,password,address,state_id,city_id,pincode,"\
                      "pan_no,is_active,created_by,created_date,updated_by,updated_date,role) values"\
                      "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,'y',%s,now(),%s,now(),'admin')"

PORTAL_ADMIN_INSERT_COUNT = "Select count(*) from portal_admin where email_id = %s and pan_no = %s"

GET_LAST_VALUE='select LAST_INSERT_ID() as id'

GET_PORTAL_ADMIN_BY_ID = "Select admin_id,first_name,last_name,phone_no,email_id,address,pa.state_id,pa.city_id,pincode,pan_no," \
                         "pa.portal_user_image,s.state_name,c.city_name " \
                         "from portal_admin as pa " \
                         "join states as s on pa.state_id = s.state_id " \
                         "join cities as c on pa.city_id=c.city_id " \
                         "where admin_id = %s "

EMAIL_COUNT = "Select count(*) from portal_admin where email_id = %s "

GET_PASSWORD = "Select password from portal_admin where phone_no = %s "

GET_INFO_BY_USER_PWD = "Select admin_id,first_name,last_name,phone_no,email_id,address,ve.state_id,ve.city_id,pincode,pan_no"\
                        ",portal_user_image,role,s.state_name,c.city_name " \
                        "from portal_admin as ve " \
                        "join states as s on ve.state_id = s.state_id " \
                        "join cities as c on ve.city_id=c.city_id " \
                        "where phone_no = %s and password = %s "
UPDATE_PORTAL_ADMIN_PROFILE = "Update portal_admin set first_name = %s,last_name = %s,phone_no = %s,address = %s, " \
                            "state_id=%s,city_id= %s,pincode=%s,portal_user_image = % s,updated_by = %s," \
                            " updated_date = now() Where admin_id = %s"

UPDATE_PASSWORD = "update portal_admin set password = %s where email_id = %s"

UPDATE_PORTAL_ADMIN_DETAILS = "Update portal_admin set first_name = %s,last_name = %s,phone_no = %s,email_id = %s,address = %s,"\
                              "state_id=%s,city_id= %s,pincode=%s, pan_no = %s ,updated_by = %s, updated_date = now() "\
                              "Where admin_id = %s"

PORTAL_ADMIN_UPDATE_COUNT = "Select count(*) from portal_admin where pan_no = %s and email_id = %s and admin_id != %s"

MOBILENUM_COUNT = 'SELECT COUNT(phone_no) from portal_admin WHERE phone_no=%s'