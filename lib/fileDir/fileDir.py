import shutil
import os, errno
import traceback

def deleteDirectory(DirectoryPath):
    shutil.rmtree(DirectoryPath)

def createNewDirectoryS(DirectoryPath):
    if os.path.exists(DirectoryPath):
        deleteDirectory(DirectoryPath)
    if not os.path.exists(DirectoryPath):
        os.makedirs(DirectoryPath)

def createDirectoryIfNotExists(DirectoryPath):
    if not os.path.exists(DirectoryPath):
        os.makedirs(DirectoryPath)

def deleteFile(FilePath):
    try:
        os.remove(FilePath)
    except OSError as e:  # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT:  # errno.ENOENT = no such file or directory
            raise  # re-raise exception if a different error occurred


''' Write json string to the file '''
def writeJSONtofile(json_string, json_file_path):
    try:
        with open(json_file_path,
                  'a+') as outfile:
            outfile.write(json_string)
            outfile.close()
    except Exception as err:
        raise

''' create file '''
def createFile(file_path):
    try:
        fs = open(file_path, "w+", encoding='utf-8')
        fs.close()
    except Exception as err:
        raise