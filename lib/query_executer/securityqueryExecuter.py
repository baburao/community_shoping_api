"""
Name                       : Query Execute
Description                : It execute queries and returns rows.
Created By                 : Bhanu Tej P
Created Date               : 14-05-2018
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""
from django.db import connections

securityConn = 'security'
class SecurityQueryExecute(object):
    def execute(args, query):
        cursor = connections[securityConn].cursor()
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        rows = cursor.fetchall()
        cursor.close()
        result = rows[0][0]
        if result is None:
            return []
        else:
            return result


    def getCursor(self):
        return connections[securityConn].cursor()

    def execute_many(args, query, cursor):
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        rows = cursor.fetchall()
        result = rows[0][0]
        if result is None:
            return []
        else:
            return result

    def insertexecute(args, query):
        cursor = connections[securityConn].cursor()
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        return
