"""
Name                       : Query Execute
Description                : It execute queries and returns rows.
Created By                 : Bhanu Tej P
Created Date               : 14-05-2018
Last Modified By           :
Last Modified Date         :
Modification Description   :
"""
from django.db import connection


class QueryExecute(object):
    def __init__(self):
        print('QueryExecute')

    def execute(args, query):
        cursor = connection.cursor()
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        rows = cursor.fetchall()
        cursor.close()
        result = rows[0][0]
        if result is None:
            return []
        else:
            return result

    def execute_many(args, query, cursor):
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        rows = cursor.fetchall()
        if rows:
            result = rows[0][0]
            if result is None:
                return []
            else:
                return result
        else:
            return []

    def insert_execute(args, query):
        cursor = connection.cursor()
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        cursor.close()

    def insert_execute_many(args, query, cursor):
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)

    def get_json(args, query):
        cursor = connection.cursor()
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        result = cursor.fetchall()
        if result is None:
            return []
        else:
            return [dict(zip([key[0] for key in cursor.description], row)) for row in result]
        cursor.close()

    def get_json_many(args, query, cursor):
        if len(args) == 0:
            cursor.execute(query)
        else:
            cursor.execute(query, args)
        result = cursor.fetchall()
        if result is None:
            return []
        else:
            return [dict(zip([key[0] for key in cursor.description], row)) for row in result]

    result_insert = 'Inserted'
    result_duplicate = 'Duplicate'
    result_update = 'Updated'
    result_cancelled = 'Cancelled'

    # def insert_execute_many_with_json_object(args, query, cursor):
    #     if args != {}:
    #         query = query % args
    #     cursor.execute(query)
    #
    # def get_mdm_connection_string(self):
    #     USER = settings.DATABASES["default"]["USER"]
    #     PASSWORD = settings.DATABASES["default"]["PASSWORD"]
    #     HOST = settings.DATABASES["default"]["HOST"]
    #     PORT = settings.DATABASES["default"]["PORT"]
    #     DBNAME = settings.DATABASES["default"]["NAME"]
    #     return "postgresql://" + USER + ":" + PASSWORD + "@" + HOST + ":" + PORT + "/" + DBNAME
