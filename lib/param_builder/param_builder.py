def GetQueryParams(keys,api_params):
    query_params = []
    keys = keys.split(',')
    for key in keys:
        query_params.append(api_params[key])
    return query_params

def GetQueryParamsWithDefaultValue(keys,api_params):
    query_params = []
    keys = keys.split(',')
    for key in keys:
        query_params.append(api_params.get(key,None))
    return query_params