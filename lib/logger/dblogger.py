import logging
from crequest.middleware import CrequestMiddleware
import json
from lib.logger.dbLogHandler import dbLogHandler

class dblogger(object):
    def __init__(self, moduleName):

        logger = logging.getLogger('simple_example')
        logger.setLevel(logging.DEBUG)

        dbLog = dbLogHandler(moduleName)
        logger.addHandler(dbLog)

        self._logger = logger

    def get(self):
        return self

    def concat_msg(self,msg):
        request = CrequestMiddleware.get_request()
        path=''
        params=''
        try:
            path = request.path
        except Exception as e:
            print(e)

        try:
            params = json.dumps(request.data['params'])
        except Exception as e:
            print(e)

        final_msg=''

        if path != '':
            final_msg = '\nAPI Service: ' + path
        if params != '':
            final_msg = final_msg + '\nAPI Service Params: ' + "\n" + params

        final_msg = final_msg + '\nLog Message: ' + "\n" + msg+ "\n\n"
        return final_msg

    def info(self,msg):
        self._logger.info(self.concat_msg(msg))
        print(msg)

    def debug(self,msg):
        self._logger.debug(self.concat_msg(msg))
        print(msg)

    def error(self,msg):
        self._logger.error(self.concat_msg(msg))
        print(msg)

    def critical(self,msg):
        self._logger.critical(self.concat_msg(msg))
        print(msg)

    def warning(self,msg):
        self._logger.warning(self.concat_msg(msg))
        print(msg)

