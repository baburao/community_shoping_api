import os
import logging
from community_shoping import settings
from crequest.middleware import CrequestMiddleware
import json

class logger(object):
    def __init__(self, moduleName):
        global functionName
        global functionParams
        functionName = ""
        functionParams = {}
        moduleName = moduleName.replace('.log','')
        logger = logging.getLogger(' ModuleName: %s' % moduleName)    # log_namespace can be replaced with your namespace
        logger.setLevel(logging.DEBUG)
        if not logger.handlers:
            file_name = os.path.join(settings.LOGGING_DIR, '%s.log' % moduleName)    # usually I keep the LOGGING_DIR defined in some global settings file
            handler = logging.FileHandler(file_name)
            formatter = logging.Formatter('%(asctime)s %(levelname)s:%(name)s %(message)s')
            handler.setFormatter(formatter)
            handler.setLevel(logging.DEBUG)
            logger.addHandler(handler)
        self._logger = logger

    def get(self):
        return self

    def concat_msg(self,msg):
        final_msg = ''
        try:
            request = CrequestMiddleware.get_request()
            path=''
            params=''
            try:
                path = request.path
            except Exception as e:
                print(e)

            try:
                params = json.dumps(request.data['params'])
            except Exception as e:
                try:
                    params = json.dumps(request.data)
                except Exception as e:
                    params = ''

            if path != '':
                final_msg = '\nAPI Service: ' + path
            if params != '':
                final_msg = final_msg + '\nAPI Service Params: ' + "\n" + params

            final_msg = final_msg + '\nLog Message: ' + "\n" + msg+ "\n\n"
        except Exception as e:
            final_msg = '\nLog Message: ' + "\n" + msg+ "\n\n"

        return final_msg

    def info(self,msg):
        msg = self.concat_msg(msg)
        self._logger.info(msg)
        print(msg)

    def debug(self,msg):
        self._logger.debug(self.concat_msg(msg))
        print(msg)

    def error(self,msg):
        self._logger.error(self.concat_msg(msg))
        print(msg)

    def critical(self,msg):
        self._logger.critical(self.concat_msg(msg))
        print(msg)

    def warning(self,msg):
        self._logger.warning(self.concat_msg(msg))
        print(msg)
