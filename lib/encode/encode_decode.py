from cryptography.fernet import Fernet
import hashlib
import datetime
from community_shoping import settings

# key = Fernet.generate_key()
key = (settings.CRYPTOGRAPHY_KEY).encode()
# key = ("7K3sNOuO2KRCbHCKd2zMcJp9FoQ0rqKvjM1REuCPNYs=").encode()

def encode(plain_text):
    encode_cipher_suite = Fernet(key)
    cipher_text = encode_cipher_suite.encrypt(plain_text.encode())
    return cipher_text.decode()

def decode(cipher_text):
    decode_cipher_suite = Fernet(key)
    plain_text = decode_cipher_suite.decrypt(cipher_text.encode())
    return plain_text.decode()

def encode_md5(encode_md5):
    return hashlib.md5(encode_md5.encode()).hexdigest()

if __name__ == "__main__":
    # plain_text = decode("gAAAAABbUbAUAORtPVvhgX4w8rIC0LbaVEGIU_zy4DnTgT0gprr0pouXR9Hnu9lJX8G2mhqMIIoqmz0iMN-vhqvD-BPJjxXIMw==")
    # print(plain_text)
    cipher_text = encode("oracle2")
    print(cipher_text)

