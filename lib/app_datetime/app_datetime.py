import hashlib
import datetime

def current_datetime_md5():
    rec_insrt_ts = datetime.datetime.now()
    str_rec_insrt_ts = str(rec_insrt_ts).replace(':', '_').replace('-', '_').replace('.', '_').replace(' ',
                                                                                                       '__')
    datetime_hash_id = hashlib.md5(str_rec_insrt_ts.encode()).hexdigest()
    return datetime_hash_id

def current_datetime():
    return datetime.datetime.now()

def str_current_datetime():
    strTime = str(datetime.datetime.now()).replace(':', '_').replace('-', '_').replace('.', '_').replace(' ', '__')
    return strTime
